package main

import (
	"bitbucket.org/anzellai/gox"
	"bitbucket.org/anzellai/gox/cmd/example/proto/v1ExampleAPI"
	"github.com/golang/protobuf/ptypes/wrappers"
	"google.golang.org/protobuf/types/known/wrapperspb"
)

func main() {
	g := gox.New("example-store-client")
	conn, err := g.NewClient(g.Context(), "example-store", ":"+gox.GRPCPort())
	if err != nil {
		g.Logger().Fatal(err)
	}
	client := v1ExampleAPI.NewExampleAPIServiceClient(conn)

	// calling Info to ping dependencies DB status
	info := &v1ExampleAPI.Request{}
	response, err := client.Info(g.Context(), info)
	if err != nil {
		g.Logger().Error(err)
	} else {
		g.Logger().Debugf("Info response: %v", response)
	}

	// example tasks assignment
	task1 := &v1ExampleAPI.Task{
		Uuid:      "uuid-12345",
		Title:     "example 1 testing title",
		Completed: &wrappers.BoolValue{Value: true},
	}
	task2 := &v1ExampleAPI.Task{
		Uuid:      "uuid-23456",
		Title:     "example 2 testing title",
		Completed: &wrapperspb.BoolValue{Value: false},
	}

	/// calling AddTask
	// upsert task1
	response, err = client.AddTask(g.Context(), task1)
	if err != nil {
		g.Logger().Error(err)
	} else {
		g.Logger().Debugf("AddTask response: %v", response)
	}
	// upsert task2
	response, err = client.AddTask(g.Context(), task2)
	if err != nil {
		g.Logger().Error(err)
	} else {
		g.Logger().Debugf("AddTask response: %v", response)
	}

	/// calling GetTask
	// get task1 by its uuid
	response, err = client.GetTask(g.Context(), &v1ExampleAPI.RequestBy{Uuid: task1.GetUuid()})
	if err != nil {
		g.Logger().Error(err)
	} else {
		g.Logger().Debugf("GetTask response: %v", response)
	}
	// get task2 by its uuid
	response, err = client.GetTask(g.Context(), &v1ExampleAPI.RequestBy{Uuid: task2.GetUuid()})
	if err != nil {
		g.Logger().Error(err)
	} else {
		g.Logger().Debugf("GetTask response: %v", response)
	}

	/// calling ListTasks
	// query and filter without QueryInput will return all records
	response, err = client.ListTasks(g.Context(), &v1ExampleAPI.SearchRequest{})
	if err != nil {
		g.Logger().Error(err)
	} else {
		g.Logger().Debugf("GetTask response: %v", response)
	}
	// query and filter with completed status == true, should return task1
	response, err = client.ListTasks(g.Context(), &v1ExampleAPI.SearchRequest{
		Completed: true,
	})
	if err != nil {
		g.Logger().Error(err)
	} else {
		g.Logger().Debugf("GetTask response: %v", response)
	}
	// query and filter with completed status == false, should return task2
	response, err = client.ListTasks(g.Context(), &v1ExampleAPI.SearchRequest{
		Incompleted: true,
	})
	if err != nil {
		g.Logger().Error(err)
	} else {
		g.Logger().Debugf("GetTask response: %v", response)
	}

	/// calling RemoveTask
	// remove task1 by its uuid
	response, err = client.RemoveTask(g.Context(), &v1ExampleAPI.RequestBy{Uuid: task1.GetUuid()})
	if err != nil {
		g.Logger().Error(err)
	} else {
		g.Logger().Debugf("GetTask response: %v", response)
	}
	// remove task2 by its uuid
	response, err = client.RemoveTask(g.Context(), &v1ExampleAPI.RequestBy{Uuid: task2.GetUuid()})
	if err != nil {
		g.Logger().Error(err)
	} else {
		g.Logger().Debugf("GetTask response: %v", response)
	}
}
