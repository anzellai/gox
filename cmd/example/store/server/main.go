package main

import (
	"context"
	"time"

	"bitbucket.org/anzellai/gox"
	v1ExampleAPI "bitbucket.org/anzellai/gox/cmd/example/proto/v1ExampleAPI"
	. "bitbucket.org/anzellai/gox/pkg/goxquery"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"google.golang.org/protobuf/types/known/timestamppb"
)

// G is used to implement helloworld.GreeterServer.
// it also uses embedding for direct GoX access
type G struct {
	*gox.Gox
}

func main() {
	// NOTE: using WithDB will create a scoped Gox with DBType
	// engine is initialised with a closer ready to use with full auto-logging
	// BDBEngine is a BoltDB embedded key-value storage backend
	gg, closer := gox.New("example-store-server").WithDB(gox.BDBEngine)
	defer closer()

	/*
		TRYME: if you want to use DDB or ES engine, you can start with a Localstack
		under minikube (once you have switch k8s context to minikube), by running:

		`make localstack`

		then wait until the first time to provision all resources under minikube, then:

		`make localstack-es` for creating an Elasticsearch Cluster of `localstack`
		`make localstack-ddb` for creating a DynamoDB Table with name `dbexample`

		then, under GoX, you can initialise with `UserLocalStack()` method like this:
	*/
	// gg, closer := gox.New("example-store-server").
	//     UseLocalStack().
	//     WithDB(gox.DDBEngine)
	// defer closer()

	/*
		Or, with Elasticsearch backend, you can simply do the same.
		The ES cluster will be available to http://127.0.0.1:4571

		Also, other LocalStack resources are available via Edge Port on http://127.0.0.1:4566

		And the environment variable ELASTICSEARCH_URL will be set automatically.
		Likewise environment variable LOCALSTACK_URL will be set too (if not yet in env var)
	*/
	// gg, closer := gox.New("example-store-server").
	//   UseLocalStack().
	//   WithDB(gox.ESEngine)
	// defer closer()

	g := &G{gg}
	// this will give us a context logger derived from g
	ctx, gLogger := gox.GetLogger(g.Context())

	// initialised a gRPC server with gox sane defaults (not running yet!)
	grpcServer := g.NewServer()
	v1ExampleAPI.RegisterExampleAPIServiceServer(grpcServer, g)
	// put all hooks with the grpc gateway handy http server
	mux := runtime.NewServeMux()
	if err := v1ExampleAPI.RegisterExampleAPIServiceHandlerFromEndpoint(
		ctx,
		mux,
		// gox.HTTPPort() will give you default from env var HTTP_PORT running on
		":"+gox.GRPCPort(),
		// NewDialOptions will sane defaults on client gRPC
		gox.NewDialOptions(ctx, "example-store-http-server"),
	); err != nil {
		gLogger.Fatal(err)
	}
	// initialised a http server with gox sane defaults (not running yet!)
	httpServer := g.NewHTTPServer(mux)

	/// there are a few options here
	/// 1. we can run gRPC or HTTP servers independently, with goroutines:
	///   - go g.Run(grpcServer)
	///   - go g.RunHTTP(httpServer)
	/// 2. or we run them altogether, like so:
	///   - err := g.RunAll(grpcServer, httpServer)
	///
	/// NOTE: either run them independently or altogether will feed errors to
	/// the same healthz and status, middleware and panic recovery are closely
	/// similar with gRPC and HTTP.
	g.Logger().Error(g.RunAll(grpcServer, httpServer))
}

// Info implementation
func (g *G) Info(ctx context.Context, in *v1ExampleAPI.Request) (out *v1ExampleAPI.Response, err error) {
	out = &v1ExampleAPI.Response{}
	input := &gox.Input{Index: "dbexample"}
	output, err := g.DBInfo(ctx, input)
	if err != nil {
		return
	}
	out.Info = output.Info
	return
}

// AddTask implementation
func (g *G) AddTask(ctx context.Context, in *v1ExampleAPI.Task) (out *v1ExampleAPI.Response, err error) {
	out = &v1ExampleAPI.Response{}
	in.UpdatedAt = &timestamppb.Timestamp{
		Seconds: time.Now().UTC().Unix(),
	}
	input := &gox.Input{
		Index:  "dbexample",
		Record: in,
	}
	// check if record existed in DB
	output, err := g.DBGet(ctx, input)
	if err == nil && output.Record != nil {
		mIn := &v1ExampleAPI.Task{}
		if err = gox.BindOne(output.Record, mIn); err != nil {
			return
		}
		in.CreatedAt = mIn.CreatedAt
		input.Record = in
	}
	output, err = g.DBPut(ctx, input)
	if err != nil {
		return
	}
	task := &v1ExampleAPI.Task{}
	if err = gox.BindOne(output.Record, task); err != nil {
		return
	}
	out.Task = task
	return
}

// GetTask implementation
func (g *G) GetTask(ctx context.Context, in *v1ExampleAPI.RequestBy) (out *v1ExampleAPI.Response, err error) {
	out = &v1ExampleAPI.Response{}
	input := &gox.Input{
		Index:  "dbexample",
		Record: &v1ExampleAPI.Task{Uuid: in.GetUuid()},
	}
	output, err := g.DBGet(ctx, input)
	if err != nil {
		return
	}
	task := &v1ExampleAPI.Task{}
	if err = gox.BindOne(output.Record, task); err != nil {
		return
	}
	out.Task = task
	return
}

// ListTasks implementation
func (g *G) ListTasks(ctx context.Context, in *v1ExampleAPI.SearchRequest) (out *v1ExampleAPI.Response, err error) {
	out = &v1ExampleAPI.Response{}

	// NOTE: we can construct a new root Query using goxquery DSL
	// Set Luecene custom parser to if you are using Lucene compatible engine
	// i.e. for Elasticsearch or Solr search backend
	query := NewQuery().
		SortBy("createdAt", DESC)

	// NOTE: use dynamic logic to add Term filter to root Query
	// EQ, NE, GT, GTE, LE, LTE are all available DSL for Term
	if in.GetCompleted() {
		query = query.FilterBy("completed", EQ, true)
	} else if in.GetIncompleted() {
		query = query.FilterBy("completed", EQ, false)
	}

	if in.GetTitle() != "" {
		query = query.FilterBy("title", CONTAIN, in.GetTitle())
	}

	if in.GetAfter() != "" {
		query = query.FilterBy("createdAt", GTE, in.GetAfter())
	}

	input := &gox.Input{
		Index: "dbexample",
		Query: query,
	}
	output, err := g.DBQuery(ctx, input)
	if err != nil {
		return
	}
	// BindMulti gives us a bytes walker to parse each proto.Message
	// NOTE: due to []proto.Message is a type not supported by jsonpb
	records, err := gox.BindMulti(output.Records)
	if err != nil {
		return
	}
	for record := range records {
		task := &v1ExampleAPI.Task{}
		if err = gox.BindOne(record, task); err != nil {
			return
		}
		out.Tasks = append(out.Tasks, task)
	}
	return
}

// RemoveTask implementation
func (g *G) RemoveTask(ctx context.Context, in *v1ExampleAPI.RequestBy) (out *v1ExampleAPI.Response, err error) {
	out = &v1ExampleAPI.Response{}
	input := &gox.Input{
		Index:  "dbexample",
		Record: &v1ExampleAPI.Task{Uuid: in.GetUuid()},
	}
	output, err := g.DBDelete(ctx, input)
	if err != nil {
		return
	}
	task := &v1ExampleAPI.Task{}
	if err = gox.BindOne(output.Record, task); err != nil {
		return
	}
	out.Task = task
	return
}
