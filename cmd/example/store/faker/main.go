package main

import (
	"fmt"
	"time"

	"bitbucket.org/anzellai/gox"
	"bitbucket.org/anzellai/gox/cmd/example/proto/v1ExampleAPI"
	"github.com/bxcodec/faker/v3"
)

func main() {
	g := gox.New("example-store-client")
	conn, err := g.NewClient(g.Context(), "example-store", ":"+gox.GRPCPort())
	if err != nil {
		g.Logger().Fatal(err)
	}
	client := v1ExampleAPI.NewExampleAPIServiceClient(conn)

	for {
		time.Sleep(time.Millisecond * 20)
		task := &v1ExampleAPI.Task{}
		taskB := fmt.Sprintf(`{"uuid":"%s","title":"%s","completed":%t,"createdAt":"%s","updatedAt":"%s"}`,
			faker.UUIDHyphenated(),
			faker.Sentence(),
			faker.Timeperiod() == "PM",
			time.Now().UTC().Format(time.RFC3339),
			time.Now().UTC().Format(time.RFC3339),
		)
		if err := gox.Unmarshal([]byte(taskB), task); err != nil {
			g.Logger().Error(err)
			return
		}

		response, err := client.AddTask(g.Context(), task)
		if err != nil {
			g.Logger().Error(err)
			continue
		}
		g.Logger().WithField("response", response).Info("responded")
	}
}
