package main

import (
	"fmt"
	"net/http"

	"bitbucket.org/anzellai/gox"
	"github.com/gorilla/mux"
)

func main() {
	g := gox.New("echo-http-server")

	r := mux.NewRouter()
	r.HandleFunc("/echo/{who}", func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		w.WriteHeader(http.StatusOK)
		fmt.Fprintln(w, "Hello:", vars["who"])
	})

	server := g.NewHTTPServer(r)
	g.Logger().Error(g.RunHTTP(server))
}
