package main

import (
	"context"

	"bitbucket.org/anzellai/gox"
	pb "google.golang.org/grpc/examples/helloworld/helloworld"
)

var (
	g *gox.Gox
)

// server is used to implement helloworld.GreeterServer.
type Server struct {
	pb.UnimplementedGreeterServer
}

// SayHello implements helloworld.GreeterServer
func (s *Server) SayHello(ctx context.Context, in *pb.HelloRequest) (*pb.HelloReply, error) {
	_, logger := gox.GetLogger(ctx)
	logger.WithField("in", in).Info("received")
	return &pb.HelloReply{Message: "Hello " + in.GetName()}, nil
}

func init() {
	g = gox.New("example")
}

func main() {
	// when NewServer is called, a few hooks are automatically injected
	// returned gRPC server is not run yet at this point.
	// we do not need to do anything and these should just work:
	// - opentracing for spans and propagation for all request/response
	// - gRPC reflection for easy gRPC command line tools interaction
	// - context tags propagation
	// - panic recovery and log and trace panic calls
	// - profiling gRPC endpoint ready (need to switch on) to collect data
	s := g.NewServer()
	/*
		NOTE: we can register one or more custom healthcheckers here
		comment above and uncomment below to see how it works
		kubernetes will initialise readiness probe to verify its status
		simply use your custom healthchecker to return error if health
		is considered fatal.
		grpc_health_probe is a tool Kubernetes used to scrape your server every 5s.
		reference - https://github.com/grpc-ecosystem/grpc-health-probe
		NOTE: this is why you need to be careful not to over provision calls
		to dependency services or else your healthchecks become your bloats.
		By default the health check will return SERVING whenever your gRPC server is up.
	*/
	// healthchecker := func() error {
	//     return errors.New("dependency error: do not accept more traffic")
	// }
	// s := g.NewServer(healthchecker)
	server := &Server{}
	pb.RegisterGreeterServer(s, server)

	// call Run method and pass the initialised gRPC server to run it
	// this is all what you need to do to run a fully equipped gRPC server.
	// It also covers:
	// - os signals interruption
	// - graceful shutdown
	// - gRPC listener error
	// - logging of sigterm and server error events
	if err := g.Run(s); err != nil {
		g.Logger().Fatal(err)
	}
}
