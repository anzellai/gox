package main

import (
	"bitbucket.org/anzellai/gox"
	pb "google.golang.org/grpc/examples/helloworld/helloworld"
)

const (
	port = ":8180"
)

func main() {
	g := gox.New("example")
	clientConn, err := g.NewClient(g.Context(), "example-client", port)
	if err != nil {
		g.Logger().Fatal(err.Error())
	}
	client := pb.NewGreeterClient(clientConn)
	resp, err := client.SayHello(
		g.Context(),
		&pb.HelloRequest{Name: "World"},
	)
	if err != nil {
		g.Logger().Error(err)
	} else {
		g.Logger().WithField("message", resp.GetMessage()).Info("responded")
	}
}
