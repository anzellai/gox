package gox

/// Logger as a proxy interface to support goxlogger.Logger interface
/// this will provide a gox package level access to all
/// goxlogger exported functions, methods and types.

import (
	"context"

	"bitbucket.org/anzellai/gox/pkg/goxlogger"
)

// Logger type definition
type Logger = goxlogger.Logger

// Fields shorthand type definition
type Fields = goxlogger.Fields

// GetLogger return a logger extracted from context or a new logger
func GetLogger(ctx context.Context) (context.Context, *Logger) {
	return goxlogger.GetLogger(ctx)
}

// CtxLogger return a context has logger value
func CtxLogger(ctx context.Context, logger *Logger) context.Context {
	return goxlogger.CtxLogger(ctx, logger)
}

// NewLogger return an initialised Logger
func NewLogger() *Logger {
	return goxlogger.NewLogger()
}
