package gox

/// datastore.go is used as an interface to access multiple storage backends.
/// NOTE: we should also provide TTL hook for trigger based storage backends for usefulness.

import (
	"context"
	"time"

	"bitbucket.org/anzellai/gox/pkg/bdbstore"
	"bitbucket.org/anzellai/gox/pkg/ddbstore"
	"bitbucket.org/anzellai/gox/pkg/esstore"
	"bitbucket.org/anzellai/gox/pkg/goxstore"
	"bitbucket.org/anzellai/gox/pkg/knsstore"
	"bitbucket.org/anzellai/gox/pkg/mapstore"
	"bitbucket.org/anzellai/gox/pkg/pbsession"
	"bitbucket.org/anzellai/gox/pkg/snsstore"
	"github.com/aws/aws-sdk-go/aws/session"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var (
	ErrUninitialised = status.Error(codes.FailedPrecondition, "gox db engine is not initialised")
)

// DBType enum type for DB storage engine
type DBType uint

const (
	// NoopEngine as NoopStorer backend, do nothing basically
	NoopEngine DBType = iota
	// DDBEngine as DynamoDB backend
	DDBEngine
	// ESEngine as Elasticsearch backend
	ESEngine
	// KNSEngine as Kinesis backend
	KNSEngine
	// SNSEngine as SNS backend
	SNSEngine
	// BDBEngine as BoltDB backend
	BDBEngine
	// MAPEngine as simple in memory map backend
	MAPEngine
	// CustomEngine as custom engine type
	CustomEngine
)

// Input type definition for use in GoxStorer interface
type Input = goxstore.Input

// Output type definition for use in GoxStorer interface
type Output = goxstore.Output

// DBCloser function type signature
type DBCloser = func() error

// SetSession set given session as underlying reusable aws session
func (g *Gox) SetSession(sess *session.Session) {
	g.sess = sess
}

// WithEngine return initialised protostore.Protostore
// with provided engine as CustomEngine DBType
func (g *Gox) WithEngine(engine goxstore.GoxStorer) (*Gox, DBCloser) {
	newG, closer := g.WithDB(CustomEngine)
	newG.store = engine
	return newG, closer
}

// DB return initialised protostore.ProtoStore ready for CRUD ops
// this creates a new G proxy, it is up to user if they want this
// as global by replacing existing g, or created a new scope g.
func (g *Gox) WithDB(dbType DBType) (*Gox, DBCloser) {
	if g.sess == nil {
		sess := pbsession.Must(pbsession.New())
		g.SetSession(sess)
	}
	newG := g.Copy()

	switch dbType {
	case NoopEngine:
		newG.store = goxstore.New()
		newG.logger = newG.Logger().Namespace("NoopEngine")
		newG.ctx = CtxLogger(newG.ctx, newG.Logger())
	case DDBEngine:
		newG.store = ddbstore.New()
		newG.logger = newG.Logger().Namespace("DDBEngine")
		newG.ctx = CtxLogger(newG.ctx, newG.Logger())
	case ESEngine:
		newG.store = esstore.New()
		newG.logger = newG.Logger().Namespace("ESEngine")
		newG.ctx = CtxLogger(newG.ctx, newG.Logger())
	case KNSEngine:
		newG.store = knsstore.New()
		newG.logger = newG.Logger().Namespace("KNSEngine")
		newG.ctx = CtxLogger(newG.ctx, newG.Logger())
	case SNSEngine:
		newG.store = snsstore.New()
		newG.logger = newG.Logger().Namespace("SNSEngine")
		newG.ctx = CtxLogger(newG.ctx, newG.Logger())
	case BDBEngine:
		newG.store = bdbstore.New()
		newG.logger = newG.Logger().Namespace("BDBEngine")
		newG.ctx = CtxLogger(newG.ctx, newG.Logger())
	case MAPEngine:
		newG.store = mapstore.New()
		newG.logger = newG.Logger().Namespace("MAPEngine")
		newG.ctx = CtxLogger(newG.ctx, newG.Logger())
	default:
		newG.store = goxstore.New()
		newG.logger = newG.Logger().Namespace("CustomEngine")
		newG.ctx = CtxLogger(newG.ctx, newG.Logger())
	}
	return newG, newG.DBClose
}

// DBClose for DB clean up and finaliser
func (g *Gox) DBClose() error {
	if g.store == nil {
		return nil
	}
	err := g.store.Close()
	if err != nil {
		g.Logger().WithField("DBClose", g.dbType).Error(err)
		return err
	}
	return nil
}

// DBLogger return new logger with DBindex field scoped
func (g *Gox) DBLogger(ctx context.Context, index string) *Logger {
	_, logger := GetLogger(ctx)
	return logger.Namespace(index)
}

// DBInfo will ping for DBType backend for liveness
func (g *Gox) DBInfo(ctx context.Context, in *Input) (out *Output, err error) {
	dbLogger := g.DBLogger(ctx, "DBInfo").WithFields(Fields{
		"db.input": in.JSON(),
		"db.start": time.Now().Format(time.RFC3339),
	})
	defer func() {
		// we only log Info error
		if err != nil {
			dbLogger.WithField("db.finish", time.Now().Format(time.RFC3339)).Error(err)
		}
	}()
	ctx = CtxLogger(ctx, dbLogger)
	return g.store.Info(ctx, in)
}

// DBPut will create or update from proto.Message type into underlying DBType backend
// index argument as in ddb table, es index etc.
func (g *Gox) DBPut(ctx context.Context, in *Input) (out *Output, err error) {
	dbLogger := g.DBLogger(ctx, "DBPut").WithFields(Fields{
		"db.input": in.JSON(),
		"db.start": time.Now().Format(time.RFC3339),
	})
	defer func() {
		dbLogger = dbLogger.WithField("db.finish", time.Now().Format(time.RFC3339))
		if err != nil {
			dbLogger.Error(err)
		} else {
			dbLogger.Info("OK")
		}
	}()
	ctx = CtxLogger(ctx, dbLogger)
	return g.store.Put(ctx, in)
}

// DBPutBatch will create or update from proto.Message types into underlying DBType backend
// index argument as in ddb table, es index etc.
// NOTE: due to batch nature, output will only contain Count
func (g *Gox) DBPutBatch(ctx context.Context, in *Input) (out *Output, err error) {
	dbLogger := g.DBLogger(ctx, "DBPutBatch").WithFields(Fields{
		"db.input": in.JSON(),
		"db.start": time.Now().Format(time.RFC3339),
	})
	defer func() {
		dbLogger = dbLogger.WithField("db.finish", time.Now().Format(time.RFC3339))
		if err != nil {
			dbLogger.Error(err)
		} else {
			dbLogger.Info("OK")
		}
	}()
	ctx = CtxLogger(ctx, dbLogger)
	return g.store.PutBatch(ctx, in)
}

// DBGet will get from proto.Message type uuid field into underlying DBType backend
// index argument as in ddb table, es index etc.
func (g *Gox) DBGet(ctx context.Context, in *Input) (out *Output, err error) {
	dbLogger := g.DBLogger(ctx, "DBGet").WithFields(Fields{
		"db.input": in.JSON(),
		"db.start": time.Now().Format(time.RFC3339),
	})
	defer func() {
		dbLogger = dbLogger.WithField("db.finish", time.Now().Format(time.RFC3339))
		if err != nil {
			dbLogger.Error(err)
		} else {
			dbLogger.Info("OK")
		}
	}()
	ctx = CtxLogger(ctx, dbLogger)
	return g.store.Get(ctx, in)
}

// DBDelete will delete from proto.Message type uuid field into underlying DBType backend
// index argument as in ddb table, es index etc.
func (g *Gox) DBDelete(ctx context.Context, in *Input) (out *Output, err error) {
	dbLogger := g.DBLogger(ctx, "DBDelete").WithFields(Fields{
		"db.input": in.JSON(),
		"db.start": time.Now().Format(time.RFC3339),
	})
	defer func() {
		dbLogger = dbLogger.WithField("db.finish", time.Now().Format(time.RFC3339))
		if err != nil {
			dbLogger.Error(err)
		} else {
			dbLogger.Info("OK")
		}
	}()
	ctx = CtxLogger(ctx, dbLogger)
	return g.store.Delete(ctx, in)
}

// DBDeleteBatch will delete from proto.Message types into underlying DBType backend
// index argument as in ddb table, es index etc.
// NOTE: due to batch nature, output will only contain Count
func (g *Gox) DBDeleteBatch(ctx context.Context, in *Input) (out *Output, err error) {
	dbLogger := g.DBLogger(ctx, "DBDeleteBatch").WithFields(Fields{
		"db.input": in.JSON(),
		"db.start": time.Now().Format(time.RFC3339),
	})
	defer func() {
		dbLogger = dbLogger.WithField("db.finish", time.Now().Format(time.RFC3339))
		if err != nil {
			dbLogger.Error(err)
		} else {
			dbLogger.Info("OK")
		}
	}()
	ctx = CtxLogger(ctx, dbLogger)
	return g.store.DeleteBatch(ctx, in)
}

// DBQuery will query from DBType backend based on filter or raw bytes.
// index argument as in ddb table, es index etc.
func (g *Gox) DBQuery(ctx context.Context, in *Input) (out *Output, err error) {
	dbLogger := g.DBLogger(ctx, "DBQuery").WithFields(Fields{
		"db.input": in.JSON(),
		"db.start": time.Now().Format(time.RFC3339),
	})
	defer func() {
		dbLogger = dbLogger.WithField("db.finish", time.Now().Format(time.RFC3339))
		if err != nil {
			dbLogger.Error(err)
		} else {
			dbLogger.Info("OK")
		}
	}()
	ctx = CtxLogger(ctx, dbLogger)
	return g.store.Query(ctx, in)
}
