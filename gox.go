package gox

import (
	"context"
	"fmt"
	"io"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"runtime"
	"sync"
	"syscall"
	"time"

	"bitbucket.org/anzellai/gox/pkg/goxstore"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/health"
	"google.golang.org/grpc/reflection"
	"google.golang.org/grpc/status"

	healthpb "google.golang.org/grpc/health/grpc_health_v1"
	profsvc "google.golang.org/grpc/profiling/service"

	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpc_zap "github.com/grpc-ecosystem/go-grpc-middleware/logging/zap"
	"github.com/grpc-ecosystem/go-grpc-middleware/logging/zap/ctxzap"

	grpc_recovery "github.com/grpc-ecosystem/go-grpc-middleware/recovery"
	grpc_ctxtags "github.com/grpc-ecosystem/go-grpc-middleware/tags"
	grpc_opentracing "github.com/grpc-ecosystem/go-grpc-middleware/tracing/opentracing"
)

// Gox type definition, the core magic
type Gox struct {
	ctx          context.Context
	logger       *Logger
	sess         *session.Session
	store        goxstore.GoxStorer
	dbType       DBType
	namespace    string
	systemHealth string // singleton system health status

	lock        sync.RWMutex
	initialised bool
	readyz      healthpb.HealthCheckResponse_ServingStatus
	readyzChan  chan healthpb.HealthCheckResponse_ServingStatus
	healthz     *Healthz
	healthzChan chan error

	dialOptions []grpc.DialOption

	grpcServer    *grpc.Server
	httpServer    *http.Server
	healthzServer *http.Server
}

// Copy return a copy of gox instance
func (g *Gox) Copy() *Gox {
	g.lock.Lock()
	defer g.lock.Unlock()
	return &Gox{
		ctx:           g.ctx,
		logger:        g.logger,
		sess:          g.sess,
		store:         g.store,
		dbType:        g.dbType,
		namespace:     g.namespace,
		systemHealth:  g.systemHealth,
		lock:          sync.RWMutex{},
		initialised:   g.initialised,
		readyz:        g.readyz,
		readyzChan:    g.readyzChan,
		healthz:       g.healthz,
		healthzChan:   g.healthzChan,
		grpcServer:    g.grpcServer,
		httpServer:    g.httpServer,
		healthzServer: g.healthzServer,
	}
}

// IsInitialised return gox initialised status
func (g *Gox) IsInitialised() bool {
	return g.initialised
}

// Readyz to return Readyz status
func (g *Gox) Readyz() healthpb.HealthCheckResponse_ServingStatus {
	return g.readyz
}

// UpdateReadyz update readyz status
func (g *Gox) UpdateReadyz(status healthpb.HealthCheckResponse_ServingStatus) {
	g.readyzChan <- status
}

// Healthz to return Healthz information
func (g *Gox) Healthz() *Healthz {
	return g.healthz
}

// AddHealthzError converts and append the error information into healthz error
func (g *Gox) AddHealthzError(err error) {
	g.healthzChan <- err
}

// New return instantiated Gox
// with most commonly used tools and helpers
func New(namespace string) *Gox {
	ctx := context.Background()
	logger := NewLogger().Namespace(namespace).WithField("svc", namespace)
	ctx = ctxzap.ToContext(ctx, logger.Z())
	g := &Gox{
		ctx:       ctx,
		logger:    logger,
		namespace: namespace,
	}
	defer logger.Sync()
	return g
}

// UseLocalStack to set env on LocalStack Edge Port and stuff
func (g *Gox) UseLocalStack() *Gox {
	os.Setenv("USE_LOCALSTACK", "1")
	os.Setenv("AWS_PROFILE", "localstack")
	os.Setenv("AWS_REGION", "eu-west-1")
	if os.Getenv("LOCALSTACK_URL") == "" {
		os.Setenv("LOCALSTACK_URL", "http://127.0.0.1:4566")
		os.Setenv("ELASTICSEARCH_URL", "http://127.0.0.1:4571")
	}
	return g
}

// Context return default context
func (g *Gox) Context() context.Context {
	return ctxzap.ToContext(g.ctx, g.logger.Z())
}

// Logger return named logger
func (g *Gox) Logger() *Logger {
	return g.logger
}

func (g *Gox) InitHealthz(healthcheckers ...HealthChecker) {
	g.lock.Lock()
	defer g.lock.Unlock()
	if g.IsInitialised() {
		return
	}

	gLogger := g.Logger()
	if g.Healthz() == nil {
		healthz := &Healthz{
			Success:  true,
			Hostname: GetEnv("HOST", "localhost"),
			Metadata: HealthzMetadata{
				Boxname: GetEnv("SERVICE_NAME", ""),
				Build:   GetEnv("SERVICE_VERSION", ""),
			},
			Errors: make([]HealthzError, 0),
		}
		g.healthz = healthz
	}
	g.readyzChan = make(chan healthpb.HealthCheckResponse_ServingStatus, 1)
	g.healthzChan = make(chan error, 1)
	healthzLogger := g.Logger().Namespace("healthz")
	readyzLogger := g.Logger().Namespace("readyz")

	go func() {
		defer close(g.readyzChan)
		for status := range g.readyzChan {
			if g.Readyz() != status {
				if status == healthpb.HealthCheckResponse_SERVING {
					readyzLogger.Infof("readiness upgraded to: %s", status.String())
				} else {
					readyzLogger.Errorf("readiness degraded to: %s", status.String())
				}
			}
			g.readyz = status
		}
	}()
	gLogger.Info("registered readyz status change handler")

	go func() {
		if len(healthcheckers) == 0 {
			healthcheckers = []HealthChecker{defaultHealthChecker}
		}
		// asynchronously inspect dependencies and toggle serving status as needed
		next := healthpb.HealthCheckResponse_UNKNOWN
		g.UpdateReadyz(next)
		for {
			time.Sleep(time.Second * 5)
			for _, healthchecker := range healthcheckers {
				if err := healthchecker(); err != nil {
					readyzLogger.Errorf("readiness healthchecker error: %v", err)
					next = healthpb.HealthCheckResponse_NOT_SERVING
					break
				} else {
					next = healthpb.HealthCheckResponse_SERVING
				}
			}
			if g.store != nil {
				if _, err := g.store.Info(g.Context(), nil); err != nil {
					readyzLogger.Errorf("DBInfo healthchecker error: %v", err)
					next = healthpb.HealthCheckResponse_NOT_SERVING
					g.AddHealthzError(err)
				}
			}
			g.UpdateReadyz(next)
		}
	}()
	gLogger.Info("registered readyz checkers")

	go func() {
		defer close(g.healthzChan)
		for err := range g.healthzChan {
			if err == nil {
				continue
			}
			gLogger.Namespace("healthz").Error(ToHealthzError(err))
			healthzError := ToHealthzError(err)
			if len(g.healthz.Errors) > 99 {
				g.healthz.Errors = g.healthz.Errors[:99]
			}
			g.healthz.Errors = append([]HealthzError{healthzError}, g.healthz.Errors...)
		}
	}()
	gLogger.Info("registered healthz error handler")

	go func() {
		healthzPort := HealthzHTTPPort()

		r := mux.NewRouter()
		r.HandleFunc("/readyz", func(w http.ResponseWriter, r *http.Request) {
			if g.Readyz() == healthpb.HealthCheckResponse_SERVING {
				w.WriteHeader(http.StatusOK)
			} else {
				// only error log when readiness is not SERVING
				readyzLogger.Errorf("readyz status returning: %s", g.Readyz().String())
				w.WriteHeader(http.StatusServiceUnavailable)
			}
		})
		readyzLogger.Infof("registered readyz http endpoint on port: %s", healthzPort)

		r.HandleFunc("/healthz", func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Content-Type", "application/json")
			w.Header().Set("X-Content-Type-Options", "nosniff")
			w.WriteHeader(http.StatusOK)
			b, err := Marshal(g.Healthz())
			if err != nil {
				w.Write([]byte(fmt.Sprintf(
					`{"success":true,"errors":[{"error":"%s","description":"cannot unmarshal healthz"}]}`,
					err.Error(),
				)))
			} else {
				w.Write(b)
			}
		})
		healthzLogger.Infof("registered healthz http endpoint on port: %s", healthzPort)
		healthzServer := &http.Server{
			Handler:      r,
			Addr:         ":" + healthzPort,
			WriteTimeout: 10 * time.Second,
			ReadTimeout:  10 * time.Second,
		}
		g.healthzServer = healthzServer
		healthzLogger.Error(g.healthzServer.ListenAndServe())
	}()
	g.initialised = true
	gLogger.Info("registered and serving healthz and readyz endpoints")
}

func (g *Gox) runAll() error {
	gLogger := g.Logger()
	gLogger.Sync()

	errChan := make(chan error, 1)
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		sig := <-sigChan
		gSignal := gLogger.WithField("signal", sig)
		gSignal.Info("signal received")
		if g.IsInitialised() && g.healthzServer != nil {
			ctx, cancel := context.WithTimeout(g.Context(), time.Second*5)
			defer cancel()
			if err := g.healthzServer.Shutdown(ctx); err != nil {
				errChan <- err
			} else {
				gSignal.Info("healthz server stopped gracefully")
			}
		}
		if g.httpServer != nil {
			ctx, cancel := context.WithTimeout(g.Context(), time.Second*5)
			defer cancel()
			if err := g.httpServer.Shutdown(ctx); err != nil {
				errChan <- err
			}
			gSignal.Info("http server stopped gracefully")
		}
		if g.grpcServer != nil {
			g.grpcServer.GracefulStop()
			gSignal.Info("gRPC server stopped gracefully")
		}

		select {
		case errChan <- nil:
		default:
		}
	}()
	gLogger.Info("registered signal interruption handler")

	if g.grpcServer != nil {
		grpcPort := GRPCPort()
		go func() {
			lis, err := net.Listen("tcp", ":"+grpcPort)
			if err != nil {
				gLogger.Errorf("net.Listen:error: %v", err)
				errChan <- err
			}
			err = g.grpcServer.Serve(lis)
			if err == grpc.ErrServerStopped {
				err = nil
			} else {
				gLogger.Errorf("grpc.Serve:error: %v", err)
			}
			select {
			case errChan <- err:
				g.AddHealthzError(err)
			default:
			}
		}()
		gLogger.Infof("gRPC server running on: %v", grpcPort)
	}

	if g.httpServer != nil {
		go func() {
			err := g.httpServer.ListenAndServe()
			if err != nil {
				gLogger.Errorf("http.ListenAndServe:error: %v", err)
				errChan <- err
			}
			select {
			case errChan <- err:
				g.AddHealthzError(err)
			default:
			}
		}()
		gLogger.Infof("http server running on: %v", g.httpServer.Addr)
	}
	g.InitHealthz()

	return <-errChan
}

// RunAll register both gRPC and HTTP servers and run in a single function
// Good for a single server running gRPC and HTTP with grpc gateway endpoints
func (g *Gox) RunAll(grpcServer *grpc.Server, httpServer *http.Server) error {
	g.grpcServer = grpcServer
	g.httpServer = httpServer
	return g.runAll()
}

// RunHTTP serve HTTP server from env var HTTP_PORT or default 8080
// this is a blocking call until SIGTERM or error is received.
func (g *Gox) RunHTTP(server *http.Server) error {
	g.httpServer = server
	return g.runAll()
}

// NewHTTPServer return a new and namespaced http server
func (g *Gox) NewHTTPServer(handler http.Handler, healthcheckers ...HealthChecker) *http.Server {
	serverLogger := g.Logger().Namespace("httpServer")
	defer serverLogger.Sync()
	// apart from normal HTTP methods, we should skip logging them
	logMethods := []string{
		http.MethodConnect,
		http.MethodDelete,
		http.MethodGet,
		http.MethodHead,
		http.MethodOptions,
		http.MethodPost,
		http.MethodPut,
		http.MethodTrace,
	}
	// inject goxlogger as middleware of request
	goxFormatter := func(writer io.Writer, params handlers.LogFormatterParams) {
		shouldLog := false
		for _, allowed := range logMethods {
			if params.Request.Method == allowed {
				shouldLog = true
				break
			}
		}
		if !shouldLog {
			return
		}
		gLogger, ok := writer.(*Logger)
		if ok {
			gLogger.WithFields(Fields{
				"http.headers": params.Request.Header,
				"http.method":  params.Request.Method,
				"http.url":     params.URL.String(),
				"http.size":    params.Size,
				"http.code":    params.StatusCode,
				"http.status":  http.StatusText(params.StatusCode),
			}).Info("handled")
		} else {
			// should never happen, but never be so certain
			msg := fmt.Sprintf(
				"%s [%d] [%s] %s - %d",
				params.TimeStamp.Format(time.RFC3339),
				params.StatusCode,
				params.Request.Method,
				params.URL.Path,
				params.Size,
			)
			writer.Write([]byte(msg))
		}
	}
	recovery := func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			defer func() {
				err := recover()
				if err != nil {
					panicError := status.Errorf(codes.Internal, "%s", err)
					g.AddHealthzError(panicError)
					b := make([]byte, 1<<16)
					ssize := runtime.Stack(b, true)
					serverLogger.WithFields(Fields{
						"panic": err,
						"numGo": runtime.NumGoroutine(),
						"stack": string(b[:ssize]),
					}).Error(panicError.Error())
					runtime.GC()
				}
			}()
			next.ServeHTTP(w, r)
		})
	}

	h := recovery(handlers.CustomLoggingHandler(serverLogger, handler, goxFormatter))

	httpPort := HTTPPort()
	server := &http.Server{
		Addr:         ":" + httpPort,
		Handler:      h,
		ReadTimeout:  time.Second * 30,
		WriteTimeout: time.Second * 30,
	}
	errLogger := log.New(serverLogger.ErrorLogger(), "ErrorLog", log.LstdFlags)
	server.ErrorLog = errLogger
	return server
}

// Run will serve gRPC server from env var GRPC_PORT or default 8180
// this is a blocking call until SIGTERM or error is received.
func (g *Gox) Run(server *grpc.Server) error {
	g.grpcServer = server
	return g.runAll()
}

// NewServer return a new and namespaced grpc server
func (g *Gox) NewServer(healthcheckers ...HealthChecker) *grpc.Server {
	serverLogger := g.Logger().Namespace("grpcServer")
	defer serverLogger.Sync()

	// NOTE: do not log the payload in/out under production environment
	decider := func(context.Context, string, interface{}) bool {
		return LoggingPayloadDecider()
	}

	// NOTE: register a recovery function to reflect current state of program
	// also force a GC pause to release hanging resources AFTER the stacktrace logs
	recovery := func(ctx context.Context, p interface{}) error {
		_, panicLogger := GetLogger(ctx)
		panicError := status.Errorf(codes.Internal, "%s", p)
		g.AddHealthzError(panicError)
		b := make([]byte, 1<<16)
		ssize := runtime.Stack(b, true)
		panicLogger.WithFields(Fields{
			"panic": p,
			"numGo": runtime.NumGoroutine(),
			"stack": string(b[:ssize]),
		}).Error(panicError)
		runtime.GC()
		return panicError
	}

	server := grpc.NewServer(
		grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(
			grpc_ctxtags.UnaryServerInterceptor(
				grpc_ctxtags.WithFieldExtractor(grpc_ctxtags.CodeGenRequestFieldExtractor),
			),
			grpc_opentracing.UnaryServerInterceptor(),
			grpc_zap.UnaryServerInterceptor(serverLogger.Z()),
			grpc_zap.PayloadUnaryServerInterceptor(serverLogger.Z(), decider),
			grpc_recovery.UnaryServerInterceptor(grpc_recovery.WithRecoveryHandlerContext(recovery)),
		)),
		grpc.StreamInterceptor(grpc_middleware.ChainStreamServer(
			grpc_ctxtags.StreamServerInterceptor(
				grpc_ctxtags.WithFieldExtractor(grpc_ctxtags.CodeGenRequestFieldExtractor),
			),
			grpc_opentracing.StreamServerInterceptor(),
			grpc_zap.StreamServerInterceptor(serverLogger.Z()),
			grpc_zap.PayloadStreamServerInterceptor(serverLogger.Z(), decider),
			grpc_recovery.StreamServerInterceptor(grpc_recovery.WithRecoveryHandlerContext(recovery)),
		)),
	)

	// setup grpc healthz probe
	healthcheck := health.NewServer()
	healthpb.RegisterHealthServer(server, healthcheck)
	go func() {
		g.InitHealthz(healthcheckers...)
		for {
			next := g.Readyz()
			healthcheck.SetServingStatus(g.systemHealth, next)
			time.Sleep(time.Second * 5)
		}
	}()

	// Include this to register a profiling-specific service within your server.
	if err := profsvc.Init(&profsvc.ProfilingConfig{Server: server}); err != nil {
		g.Logger().Error("profiling service cannot be registered", err)
	}
	// Register reflection service on gRPC server.
	reflection.Register(server)
	return server
}

// NewClient return a new and namespaced grpc client
func (g *Gox) NewClient(ctx context.Context, svc, addr string) (*grpc.ClientConn, error) {
	return NewClient(ctx, svc, addr)
}

// NewDialOptions return default grpc dial options for used in client
// also useful for grpc gateway registering server endpoint proxy
func NewDialOptions(ctx context.Context, svc string) []grpc.DialOption {
	_, logger := GetLogger(ctx)
	clientLogger := logger.Namespace(svc).WithField("svc", svc)
	defer clientLogger.Sync()
	// NOTE: do not log the payload in/out under production environment
	decider := func(context.Context, string) bool {
		return LoggingPayloadDecider()
	}
	return []grpc.DialOption{
		grpc.WithInsecure(),
		grpc.WithMaxMsgSize(1024 * 1024 * 128),
		grpc.WithChainUnaryInterceptor(grpc_middleware.ChainUnaryClient(
			grpc_opentracing.UnaryClientInterceptor(),
			grpc_zap.UnaryClientInterceptor(clientLogger.Z()),
			grpc_zap.PayloadUnaryClientInterceptor(clientLogger.Z(), decider),
		)),
		grpc.WithChainStreamInterceptor(grpc_middleware.ChainStreamClient(
			grpc_opentracing.StreamClientInterceptor(),
			grpc_zap.StreamClientInterceptor(clientLogger.Z()),
			grpc_zap.PayloadStreamClientInterceptor(clientLogger.Z(), decider),
		)),
	}
}

// NewClient return instantiated grpc client
// with most commonly used tools and hooks ready, all with sane defaults
// NOTE: for advance usage, you can override dialOptions to fit your use case
func NewClient(ctx context.Context, svc, addr string, dialOptions ...grpc.DialOption) (*grpc.ClientConn, error) {
	newCtx, _ := GetLogger(ctx)
	if len(dialOptions) == 0 {
		dialOptions = NewDialOptions(ctx, svc)
	}
	return grpc.DialContext(
		newCtx,
		addr,
		dialOptions...,
	)
}
