package gox

import (
	"time"

	"google.golang.org/grpc/status"
)

// ToHealthzError converts from error to HealthzError
func ToHealthzError(err error) HealthzError {
	healthzError := HealthzError{Error: err.Error(), Timestamp: time.Now().UTC().Unix()}
	if status, ok := status.FromError(err); ok {
		healthzError.Description = status.Message()
	}
	return healthzError
}

// Healthz type definition to be served to /healthz endpoint
type Healthz struct {
	Success  bool            `json:"success"`
	Hostname string          `json:"hostname,omitempty"`
	Metadata HealthzMetadata `json:"metadata,omitempty"`
	Errors   []HealthzError  `json:"errors,omitempty"`
}

// HealthzMetadata type definition as embedded metadata
type HealthzMetadata struct {
	Boxname string `json:"boxname,omitempty"`
	Build   string `json:"build"`
}

// HealthzError type definition as embedded Healthz errors
type HealthzError struct {
	Error       string `json:"error"`
	Description string `json:"description,omitempty"`
	Timestamp   int64  `json:"timestamp,omitempty"`
}

// HealthChecker function signature
// to check if service is ready and healthy
type HealthChecker = func() error

// defaultHealthChecker is used as placeholder to switch health status to SERVING
func defaultHealthChecker() error {
	return nil
}
