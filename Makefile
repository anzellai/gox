clean:
	rm -rf ./cmd/example/proto/*.{pb.go,pb.gw.go,swagger.json}
	rm -rf ./bin

proto: clean
	protoc -I/usr/local/include -I. \
		-I$(GOPATH)/src \
		-I${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway \
		-I$(GOPATH)/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
		--go_out=plugins=grpc:. \
		--grpc-gateway_out=logtostderr=true:. \
		--swagger_out=logtostderr=true:. \
		./cmd/example/proto/example.proto

example-build: proto
	GOOS=linux GOARCH=amd64 go build -ldflags "-s -w" \
		 -o ./bin/example-store-server \
		 ./cmd/example/store/server/main.go

localstack:
	kubectl apply -f ./kubernetes/localstack/

localstack-run:
	@echo LocalStack Edge Port will be exposed on port 4566
	@echo While Elasticsearch endpoint will be exposed on port 4571
	while true; do kubectl -n localstack port-forward 4566:4566 4571:4571; done

localstack-es:
	@echo Creating an Elasticsearch cluster with localstack domain for development
	aws es create-elasticsearch-domain --domain-name localstack \
		--region eu-west-1 \
		--endpoint http://127.0.0.1:4566

localstack-ddb:
	@echo Creating a DynamoDB table with localstack for development
	aws dynamodb create-table --table-name dbexample --region eu-west-1 \
		--attribute-definitions=AttributeName=uuid,AttributeType=S \
		--key-schema=AttributeName=uuid,KeyType=HASH \
		--provisioned-throughput=ReadCapacityUnits=100,WriteCapacityUnits=100 \
		--endpoint-url http://localhost:4566
