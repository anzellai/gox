package gox

import (
	"os"
	"strings"
)

// GetEnv return envvar from key or fallback value
func GetEnv(key, fallback string) string {
	if val := os.Getenv(key); val != "" {
		return val
	}
	return fallback
}

// GRPCPort return grpc running port from default or env var
func GRPCPort() string {
	return GetEnv("GRPC_PORT", "8180")
}

// HTTPPort return http running port from default or env var
func HTTPPort() string {
	return GetEnv("HTTP_PORT", "8080")
}

// HealthzPort return http running port for Healthz from default or env var
func HealthzHTTPPort() string {
	return GetEnv("HEALTHZ_HTTP_PORT", "6666")
}

// LoggingPayloadDecider return a decider from default or env var if we log payload in and out
func LoggingPayloadDecider() bool {
	// any true type of value from LOGGING_PAYLOAD will enable it
	shouldLog := strings.ToLower(GetEnv("LOGGING_PAYLOAD", ""))
	if shouldLog == "1" || shouldLog == "true" {
		return true
	}
	// any false type of value from LOGGING_PAYLOAD will disable it
	if shouldLog == "0" || shouldLog == "false" {
		return false
	}
	// any other situation, log it apart from production environment
	return strings.ToLower(GetEnv("ENV", GetEnv("ENVIRONMENT", "development"))) != "production"
}
