FROM alpine:latest

RUN mkdir -p /go/bin

COPY ./bin /go/bin/

CMD ["./go/bin/example-store-server"]
