# gox

Go X framework -- random naming to make it almost sound cool.

## Summary

**gox** is a minimalistic framework MVP to make devs life easier on daily works.
This includes single initialisation of a gRPC server and client, including auto
logging and tracing middleware, plus `goxstore` -- which is a CRUD and Query
datastore interface supporting Logging to console, DynamoDB, Elasticsearch,
BoltDB, in-memory Map, GraphQL (WIP) and others like Kinesis & S3 (all WIP).

Please reference to underyling storage engine providers for
[goxstore](/pkg/goxstore):

- [ddbstore](/pkg/ddbstore) DynamoDB
- [esstore](/pkg/esstore) Elasticsearch
- [bdbstore](/pkg/bdbstore) BoltDB
- [mapstore](/pkg/mapstore) in-memory Go Map

This also includes automatic `/healthz` and `/readyz` http endpoints handler.

When you use `.WithDB(XXX)`, healthz will ping on `DBInfo` method call to check
datastore health And update healthz if error is returned, BUT not to terminate
your service.

## Usage

Please refer to `example/hello/server` and `example/hello/client` for example
usage of this framework.

Simple open these 2 commands in separate terminals:

`go run ./cmd/server/hello/main.go`

`go run ./cmd/client/hello/main.go`

You can see the trace and span and logging are all working without much
involvement.

If you want to see how **goxstore** works, you can look at `example/store`
example.

Simply open these 2 commands in separate terminals:

`go run ./cmd/server/store/main.go`

`go run ./cmd/client/store/main.go`

Basically, if you want to use one of the provided datastore engines, you can do:

make `g` global with single DBEngine:

```
func main() {
	g, closer := gox.New("my-service").WithDB(gox.DDBEngine)
	defer closer()
	// your code below ...
}

```

or, use scoped ESEngine per context:

```
gES, esCloser := g.WithDB(gox.ESEngine)
defer esCloser()
```

or, use scoped BDBEngine per context:

```
gBDB, ddbCloser := g.WithDB(gox.BDBEngine)
defer bdbCloser()
```

**NOTE**

This is important to defer the `closer` on main or before a blocking call.
Otherwise your datastore connection may be closed before you serve.

When running your gRPC server with `g.Run()` method, healthz and readyz
endpoints will be served automatically:

- default running on port `:6666`
- override `HEALTHZ_HTTP_PORT` env var for both endpoints
- it also registers the `grpc_health_probe` gRPC handler
  (https://github.com/grpc-ecosystem/grpc-health-probe)

If your service is running without gRPC server, or you simply want to inject
automated healthz on existing project, you can:

```
package main

import (
	"errors"

	"bitbucket.org/anzellai/gox"
	healthpb "google.golang.org/grpc/health/grpc_health_v1"
)

func main() {
	// just run this to initialised /healthz and /readyz HTTP endpoints
	// g := gox.New("healthz").InitHealthz()

	// you can optionally pass custom healthchecker functions to it with signature `func() errror`
	g := gox.New("healthz").InitHealthz(func() error { return nil })

	// whatever else your service wants to do... k8s will work straight away
	// if you need to override readyz status or passing error information to healthz, you can
	// simply update with grpc_health_v1 HealthCheckResponse_ServingStatus
	// like so:
	g.UpdateReadyz(healthpb.HealthCheckResponse_NOT_SERVING)

	// if you need to pass error information or details from panic to healthz, you can:
	err := errors.New("something horrible happens but service is still available")
	g.AddHealthzError(err)
}
```

You can also run your service panic recover and inject the error into healthz.
So information will be collected by k8s and your devops will be happy.

---

Alternatively, if you are running a HTTP server, you can reference to
`/example/echo-http-server/main.go`. You only need to pass the `http.Handler` to
`gox`, and healthz and readyz are ready to use with `gox.RunHTTP` method. HTTP
server will be injected with error logger for connection issues automatically
through `goxlogger`. Also a logging and recovery middleware will be in place to
interact with response logging and health check status. HTTP server will also be
gracefully shutdown with healthz endpoints too.

```
package main

import (
	"fmt"
	"net/http"

	"bitbucket.org/anzellai/gox"
	"github.com/gorilla/mux"
)

func main() {
	g := gox.New("echo-http-server")

	r := mux.NewRouter()
	r.HandleFunc("/echo/{who}", func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		w.WriteHeader(http.StatusOK)
		fmt.Fprintln(w, "Hello:", vars["who"])
	})

	server := g.NewHTTPServer(r)
	g.Logger().Error(g.RunHTTP(server))
}
```

---

Then, you can simply do CRUD and Query operations interacting with
`proto.Message` interface type(s).

`Input` as in `*goxstore.Input` type exposed by proxy type `gox.Input`, with:

- `Index` for table or index name
- `Record` for single proto message type
- `Records` for multiple proto messages type
- `Query` as in `*goxquery.Query` type with useful DSLs and builder constructor
  to query against data storage

It is recommended to import the side effects when using the DSL for less typing
and more intuitive query syntax.

Currently `goxquery` supports:

- GoxParser, which is the default to parse query DSL into text based query
  string
- LuceneParser, which will parse query DSL into Elasticsearch's Lucene query
  string
- TODO: DDBParser, which should parse query DSL into DDB's UpdateItem expression
  JSON
- MAYBE: SQLParser, which should parse query DSL into MySQL/PSQL compatible
  query string

The parsers are currently written in naive Go code, because we intend to only
support 2 parsers. When the time is right, we shall re-write the parser using
**PEG**.

```go
import (
	. "bitbucket.org/anzellai/gox/pkg/goxquery"
	"bitbucket.org/anzellai/gox"
)

/*
	Doing your thing to initialise the *gox.Gox
*/

// DoQuery to illustrate how to construct query
func DoQuery(g *gox.Gox) {
	input := &gox.Input{
		Index: "index-name",
	}

	query := NewQuery().  // if you do not import side effect, you can do `goxquery.NewQuery()`
		SetParser(new(LuceneEngine)).  // (optional) use LuceneEngine to parse DSL to Lucene syntax
		SetLimit(100).
		SortBy("createdAt", DESC).
		SortBy("updatedAt", ASC).
		FilterBy("completed", EQ, true). // Add AND filter term individually
		FilterBy("title", CONTAIN, "title").
		// you can also construct nested filters by doing this
		NestedBy(AND,
			// this particular nested query will turn into RANGE query for ES
			NewTerm("createdAt", GTE, "2020-06-01"),
			NewTerm("createdAt", LT, "2021-01-01"),
		}).
		// this nested query will build OR matches
		NestedBy(OR,
			NewTerm("updatedAt", GT, "2019-12-30"),
			NewTerm("completed", NE, true),
		})

	input.query = query
	// ... now you can use DBQuery(...) to do your stuff

	// all filters are combined as AND query at parse time
	// you can debug the query with Parse()

	println(query.MustParse())  // GoxParser and LuceneParser output will be different

	// if you have a slice of maps in Go, you can evaluate straight away like this

	// items := []map[string]interface{}{
	// 	map[string]interface{}{...},
	// 	map[string]interface{}{...},
	// 	map[string]interface{}{...},
	// 	...
	// }

	// evaluate by simply passing the items into Eval method
	// queriedAndSortedItems := query.Eval(items)

}
```

After DB call, you can inspect with returned `*goxstore.Output` type, which:

- `Record` as for raw bytes of single proto message -- you can process with
  `gox.Unmarshaler`
- `Records` as for raw bytes of multiple proto messages -- you can process
  `gox.BindOne` or a bytes walkter with `gox.BindMulti`, please reference to
  `example/store` for usage 0 `Count` as for number of record(s) processed
- `Info` as for raw bytes of `Info` ping to underlying engine

---

There are also helper functions `Marshal` and `Unmarshal` to help remove the
friction working with proto message and Go types (struct or map etc.),
converting between them, in a single method.

## Customisation

Mission of GoX is to provide all-in-one and easy to use user experience, which
means:

- be opinionated and following industrial best practices to meet user
  expectations
- close to NO configuration, everything should be initialised with its sane
  defaults
- things should JUST WORK out of the box without user diving into implementation
  details
- all sub-packages should have an equivalent interface defined under `gox` root
  package
- all sub-packages should be swappable, so easy to be upgraded or replaced
  without breaking API or refactoring
- follow semvar, API breakage MUST be a major version upgrade beyond v1

### Example to implement custom datastore backend

You can reference to `goxstore.NoopStorer`, you only need to implment a package
satisfying these methods:

- Info(context.Context, *goxstore.Input) (*goxstore.Output, error)
- Get(context.Context, *goxstore.Input) (*goxstore.Output, error)
- Query(context.Context, *goxstore.Input) (*goxstore.Output, error)
- Put(context.Context, *goxstore.Input) (*goxstore.Output, error)
- PutBatch(context.Context, *goxstore.Input) (*goxstore.Output, error)
- Delete(context.Context, *goxstore.Input) (*goxstore.Output, error)
- DeleteBatch(context.Context, *goxstore.Input) (*goxstore.Output, error)

If some of the methods are not applicable to your implemented backend, simply
return an unimplemented error.

After that you only need to use it with `.WithEngine(pkg.YourStorer)`, like
this:

```
g, closer := gox.New("your-service").WithEngine(pkg.YourStorer)
defer closer()
```

Your DB backend will be namespaced to `CustomEngine`.

## Todo

Full TODO list to come later

Known TODO's so far:

- more examples, write tests
- logger interface needs some love, current implemenation only proxy to zap
  logger _Sugar_
- add `TTL` method/interface for `goxstore` and so `gox`
- `ddbstore` to implement simple `Query`
- `ddbstore.Put` and `ddbstore.PutBatch` to use DynamoDB
  [UpdateItem API](https://docs.aws.amazon.com/amazondynamodb/latest/APIReference/API_UpdateItem.html)
- implement **MySQL** backend
- implement **Redis** backend
- implement **Kinesis** backend
- implement **S3** backend
- gRPC interceptors using zap logging, should write custom middleware so zap
  logger becomes swappable
