module bitbucket.org/anzellai/gox

go 1.14

require (
	github.com/araddon/dateparse v0.0.0-20200409225146-d820a6159ab1
	github.com/aws/aws-sdk-go v1.33.6
	github.com/bxcodec/faker/v3 v3.5.0
	github.com/dgryski/dgoogauth v0.0.0-20190221195224-5a805980a5f3
	github.com/elastic/go-elasticsearch/v7 v7.8.0
	github.com/golang/protobuf v1.4.2
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/mux v1.7.4
	github.com/grpc-ecosystem/go-grpc-middleware v1.2.0
	github.com/grpc-ecosystem/grpc-gateway v1.14.6
	github.com/opentracing/opentracing-go v1.2.0 // indirect
	github.com/sha1sum/aws_signing_client v0.0.0-20200229211254-f7815c59d5c1
	github.com/tidwall/gjson v1.6.0
	github.com/tidwall/sjson v1.1.1
	go.etcd.io/bbolt v1.3.5
	go.uber.org/zap v1.15.0
	golang.org/x/net v0.0.0-20200707034311-ab3426394381 // indirect
	golang.org/x/sys v0.0.0-20200625212154-ddb9806d33ae // indirect
	golang.org/x/text v0.3.3 // indirect
	google.golang.org/genproto v0.0.0-20200715011427-11fb19a81f2c
	google.golang.org/grpc v1.30.0
	google.golang.org/grpc/examples v0.0.0-20200716203727-6e77a8b2f6a0
	google.golang.org/protobuf v1.25.0
)
