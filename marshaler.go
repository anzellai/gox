package gox

import (
	"bitbucket.org/anzellai/gox/pkg/protomsg"
	"github.com/golang/protobuf/proto"
)

// Marshal either protobuf message or Go types into bytes
func Marshal(in interface{}) ([]byte, error) {
	return protomsg.DefaultOption.Marshal(in)
}

// Unmarshal bytes into either protobuf message or Go types
func Unmarshal(in []byte, out interface{}) error {
	return protomsg.DefaultOption.Unmarshal(in, out)
}

// Clone returns a deep copy of src.
func Clone(in interface{}) interface{} {
	return protomsg.Clone(in)
}

//Merge merges src into dst, which must be messages of the same type.
func Merge(dst interface{}, src interface{}) {
	protomsg.Merge(dst, src)
}

// BindOne converts raw bytes into single proto.Message from given base type
func BindOne(record []byte, dst proto.Message) error {
	return protomsg.BindOne(record, dst)
}

// BindMulti converts raw bytes into range of dst parser from given base type
func BindMulti(records []byte) (<-chan []byte, error) {
	return protomsg.BindMulti(records)
}

// BindAll converts raw bytes into range of dst parser from given base type
func BindAll(records []byte) ([][]byte, error) {
	return protomsg.BindAll(records)
}

// Diff to compare 2 objects and returned a map with fields of different values
// always compared objA to objB, missing fields in objB will be merged into diff as nil
// objA and objB can be either map, protobuf message or non-array json marshalling types
// Diff does not go deeper than base level as nested changes are intended to pass as whole value
// Do not care about code repetition, it is better than having another function call
func Diff(objA, objB interface{}) (diff map[string]interface{}, err error) {
	return protomsg.Diff(objA, objB)
}
