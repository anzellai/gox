package pbsession

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials/stscreds"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/dgryski/dgoogauth"
)

var (
	ErrInvalidSecret = errors.New("invalid secret")
	ErrSessionIsNil  = errors.New("session is nil")
)

func genTokenCode(secret []byte) string {
	code := dgoogauth.ComputeCode(string(secret), int64(time.Now().UTC().Unix())/30)
	return fmt.Sprintf("%06d", code)
}

func getSecret() ([]byte, error) {
	secret := os.Getenv("AWS_MFA_SECRET")
	if secret != "" {
		return []byte(secret), nil
	}
	secretPath := os.Getenv("AWS_MFA_SECRET_PATH")
	if secretPath == "" {
		return nil, ErrInvalidSecret
	}
	sb, err := ioutil.ReadFile(secretPath)
	if err != nil {
		return nil, err
	}
	return sb, nil
}

// New return a Perkbox scoped provider session (AWS) ready to use
// if environment is under LOCAL, it will use a custom token provider
func New() (*session.Session, error) {
	var (
		sess *session.Session
		err  error
	)
	if os.Getenv("NO_SESSION") != "" {
		return nil, nil
	}
	switch strings.ToUpper(os.Getenv("ENV")) {
	case "LOCAL", "DEV":
		sess, err = session.NewSessionWithOptions(session.Options{
			Config:            *aws.NewConfig().WithCredentialsChainVerboseErrors(true),
			SharedConfigState: session.SharedConfigEnable,
			AssumeRoleTokenProvider: func() (string, error) {
				secret, err := getSecret()
				if err != nil {
					return stscreds.StdinTokenProvider()
				}
				return genTokenCode(secret), nil
			},
		})
		if err != nil {
			return nil, err
		}
	default:
		sess, err = session.NewSession(aws.NewConfig())
	}
	return sess, err
}

// Must enforce a Perkbox scoped provider session (AWS) ready to use or panic
func Must(sess *session.Session, err error) *session.Session {
	if err != nil {
		panic(err)
	}
	if sess == nil && os.Getenv("NO_SESSION") == "" {
		panic(ErrSessionIsNil)
	}
	return sess
}
