# pbsession

pbsession is a package to generate a scoped provider session (AWS), supporting LOCAL + DEV environment using MFA stdin token provider or oathtool to generate token code based on AWS MFA secret key.

If environment has a `NO_SESSION` set, pbsession will skip and return null session instead, convenient for non-AWS backend.

## Usage


```
import "bitbucket.org/anzellai/gox/pkg/pbsession"


func main() {
	// general usagage and pbsession will figure out which ENV is used
	// and return an AWS session using either StdinTokenProvider or oathtool generated code
	sess, err := pbsession.New()
	// or, use Must
	sess := pbsession.Must(pbsession.New())
	// ...
}
```
