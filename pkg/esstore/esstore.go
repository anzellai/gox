package esstore

import (
	"bytes"
	"context"
	"fmt"
	"io/ioutil"

	"net/http"
	"strings"

	"bitbucket.org/anzellai/gox/pkg/getset"
	"bitbucket.org/anzellai/gox/pkg/goxquery"
	"bitbucket.org/anzellai/gox/pkg/goxstore"
	"bitbucket.org/anzellai/gox/pkg/pbsession"
	"bitbucket.org/anzellai/gox/pkg/protomsg"
	"github.com/araddon/dateparse"
	"github.com/aws/aws-sdk-go/aws/session"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	v4 "github.com/aws/aws-sdk-go/aws/signer/v4"
	"github.com/sha1sum/aws_signing_client"

	es7 "github.com/elastic/go-elasticsearch/v7"
)

var (
	opt = protomsg.DefaultOption

	luceneTmpl      = `{"query":{"query_string":{"query":"%s"}},"size":%d,"sort":[%s]}`
	luceneBatchTmpl = `{"query":{"query_string":{"query":"%s"}},"size":%d,"sort":[%s],"search_after":%s}`
)

// ESStorer is an implementation of storage engine that does nothing
type ESStorer struct {
	sess     *session.Session
	esClient *es7.Client
}

// New return ESStorer pointer
func New() *ESStorer {
	return &ESStorer{}
}

// Close clean up unused session
func (s *ESStorer) Close() error {
	s.esClient = nil
	s.sess = nil
	return nil
}

// SetSession override default AWS Session
func (s *ESStorer) SetSession(sess *session.Session) *ESStorer {
	s.sess = sess
	return s
}

// SetClient override default ES7 Client
func (s *ESStorer) SetClient(client *es7.Client) *ESStorer {
	s.esClient = client
	return s
}

// AWSSigningTransport for signer awsv4 with ESStore
type AWSSigningTransport struct {
	HTTPClient *http.Client
}

// RoundTrip implementation
func (a AWSSigningTransport) RoundTrip(req *http.Request) (*http.Response, error) {
	return a.HTTPClient.Do(req)
}

// cleanseIndex ensure index name is valid for elasticsearch index
func cleanseIndex(index string) string {
	return strings.TrimSpace(strings.ToLower(index))
}

// initSession ensure ESStorer AWS session is initialised with ESClient
func (s *ESStorer) initSession() error {
	if s.sess == nil {
		sess, err := pbsession.New()
		if err != nil {
			return status.Error(codes.FailedPrecondition, err.Error())
		}
		s.sess = sess
	}
	if s.esClient == nil {
		// non AWS env
		if s.sess == nil {
			es7client, err := es7.NewDefaultClient()
			if err != nil {
				return status.Error(codes.FailedPrecondition, err.Error())
			}
			s.esClient = es7client
			return nil
		}
		signer := v4.NewSigner(s.sess.Config.Credentials)
		awsclient, err := aws_signing_client.New(signer, nil, "es", *s.sess.Config.Region)
		if err != nil {
			return status.Error(codes.FailedPrecondition, err.Error())
		}
		signingTransport := AWSSigningTransport{
			HTTPClient: awsclient,
		}
		es7client, err := es7.NewClient(es7.Config{
			Transport: http.RoundTripper(signingTransport),
		})
		if err != nil {
			return status.Error(codes.FailedPrecondition, err.Error())
		}
		s.esClient = es7client
	}
	return nil
}

// SearchResults type definition
type SearchResults struct {
	Total *int `json:"total"`
	Hits  struct {
		Hits []struct {
			ID     string                 `json:"_id"`
			Source map[string]interface{} `json:"_source"`
		}
	} `json:"hits"`
	ScrollID string `json:"_scroll_id"`
}

// Info as in optional health check info operation
func (s *ESStorer) Info(ctx context.Context, _ *goxstore.Input) (output *goxstore.Output, err error) {
	output = &goxstore.Output{}
	if err = s.initSession(); err != nil {
		return
	}
	response, err := s.esClient.Info(
		s.esClient.Info.WithContext(ctx),
		s.esClient.Info.WithErrorTrace(),
	)
	if err != nil {
		return output, status.Error(codes.FailedPrecondition, err.Error())
	}
	if response.IsError() {
		return output, status.Error(codes.FailedPrecondition, response.Status())
	}
	b, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return output, status.Error(codes.DataLoss, err.Error())
	}
	defer response.Body.Close()

	output.Info = b
	return
}

// Get by uuid from proto message, output with single Record in bytes
func (s *ESStorer) Get(ctx context.Context, input *goxstore.Input) (output *goxstore.Output, err error) {
	output = &goxstore.Output{}
	if err = s.initSession(); err != nil {
		return
	}
	uid, err := input.CheckUUID()
	if err != nil {
		return
	}

	query := goxquery.NewQuery().
		SetParser(new(goxquery.LuceneParser)).
		SetLimit(1).
		FilterBy("uuid", goxquery.EQ, uid)

	searchInput := &goxstore.Input{
		Index: input.Index,
		Key:   uid,
		Query: query,
	}
	output, err = s.Query(ctx, searchInput)
	if err != nil {
		return
	}
	if output.Count == 0 || len(output.Records) == 0 {
		return output, status.Error(codes.NotFound, codes.NotFound.String())
	}

	outs := make([]map[string]interface{}, 0)
	if err = opt.Unmarshal(output.Records, &outs); err != nil {
		return output, status.Error(codes.DataLoss, err.Error())
	}
	if len(outs) == 0 {
		return output, status.Error(codes.DataLoss, err.Error())
	}
	b, err := opt.Marshal(outs[0])
	if err != nil {
		return output, status.Error(codes.DataLoss, err.Error())
	}

	output.Record = b
	output.Count = 1
	return
}

// Query by uuid from proto message, output with slice of Records in bytes
func (s *ESStorer) Query(ctx context.Context, input *goxstore.Input) (
	output *goxstore.Output,
	err error,
) {
	output = &goxstore.Output{}

	input.Query = input.Query.SetParser(new(goxquery.LuceneParser))

	if input.Query.Limit <= 0 {
		input.Query.Limit = 10000
	}

	// reset sorting as it will not be efficient to sort anymore
	if input.Query.Limit > 9999 {
		input.Query.Sorts = []goxquery.Sort{}
	}

	luceneQuery, err := input.Query.Parse()
	if err != nil {
		err = status.Errorf(codes.InvalidArgument, err.Error())
		return output, err
	}
	if input.Query.After.Value != nil {
		afters, ok := input.Query.After.Value.([]interface{})
		if !ok {
			return output, status.Errorf(codes.InvalidArgument, "query search_after must be an array matching sorts")
		}
		for idx, after := range afters {
			if afterString, ok := after.(string); ok {
				if dt, dtErr := dateparse.ParseAny(afterString); dtErr == nil {
					afters[idx] = dt.Unix()
				}
			}
		}
		after, err := opt.Marshal(afters)
		if err != nil {
			return output, err
		}
		luceneQuery = fmt.Sprintf(
			luceneBatchTmpl,
			luceneQuery,
			input.Query.Limit,
			strings.Join(input.Query.ParseSorts(), ","),
			string(after),
		)
	} else {
		luceneQuery = fmt.Sprintf(luceneTmpl,
			luceneQuery,
			input.Query.Limit,
			strings.Join(input.Query.ParseSorts(), ","),
		)
	}

	if input.Query.Limit > 0 && input.Query.Limit < 10000 {

		res, err := s.esClient.Search(
			s.esClient.Search.WithIndex(cleanseIndex(input.Index)),
			s.esClient.Search.WithBody(bytes.NewReader([]byte(luceneQuery))),
			s.esClient.Search.WithErrorTrace(),
			s.esClient.Search.WithContext(ctx),
		)
		if err != nil {
			return output, status.Error(codes.FailedPrecondition, err.Error())
		}
		defer res.Body.Close()
		if res.IsError() {
			return output, status.Error(codes.FailedPrecondition, res.Status())
		}
		body, err := ioutil.ReadAll(res.Body)
		if err != nil {
			return output, status.Error(codes.DataLoss, err.Error())
		}

		maps := &SearchResults{}
		err = opt.Unmarshal(body, maps)
		if err != nil {
			return output, status.Error(codes.DataLoss, err.Error())
		}
		outs := make([]map[string]interface{}, 0)
		for _, each := range maps.Hits.Hits {
			outs = append(outs, each.Source)
		}
		b, err := opt.Marshal(outs)
		if err != nil {
			return output, status.Error(codes.DataLoss, err.Error())
		}

		output.Count = len(outs)
		output.Records = b

	} else {

		var (
			outs        []map[string]interface{}
			searchAfter string
		)
		scrollCtx, cancel := context.WithCancel(ctx)
		defer cancel()
		for {

			var (
				bouts      []map[string]interface{}
				nextAnchor string
				o          *goxstore.Output
			)
			if len(outs) > 0 {
				nextAnchor, err = getset.GetAsString(outs[len(outs)-1], "uuid")
				if err != nil {
					return output, status.Error(codes.DataLoss, err.Error())
				}
				if searchAfter == nextAnchor {
					break
				}
				searchAfter = nextAnchor
				input.Query = input.Query.SetAfter("uuid", searchAfter)
			}

			// construct scrollInput for ES scroll id query
			scrollInput := &goxstore.Input{
				Index: input.Index,
				Query: input.Query.SetLimit(9999),
			}
			o, err = s.Query(scrollCtx, scrollInput)
			if err != nil {
				cancel()
				return
			}

			err = opt.Unmarshal(o.Records, &bouts)
			if err != nil {
				cancel()
				return output, status.Error(codes.DataLoss, err.Error())
			}
			if len(bouts) < 1 {
				break
			}

			outs = append(outs, bouts...)
			output.Count += len(bouts)
			bouts = nil
		}

		b, err := opt.Marshal(outs)
		if err != nil {
			return output, status.Error(codes.DataLoss, err.Error())
		}
		output.Records = b

	}

	return
}

// Put by uuid to upsert given proto message, output with slice of Records in bytes
func (s *ESStorer) Put(ctx context.Context, input *goxstore.Input) (output *goxstore.Output, err error) {
	output = &goxstore.Output{}
	if err = s.initSession(); err != nil {
		return
	}
	uid, err := input.CheckUUID()
	if err != nil {
		return
	}

	b, err := opt.Marshal(input.Record)
	if err != nil {
		return
	}
	res, err := s.esClient.Index(
		cleanseIndex(input.Index),
		bytes.NewReader(b),
		s.esClient.Index.WithRefresh("true"),
		s.esClient.Index.WithDocumentID(uid),
		s.esClient.Index.WithErrorTrace(),
		s.esClient.Index.WithContext(ctx),
	)
	if err != nil {
		return output, status.Error(codes.FailedPrecondition, err.Error())
	}
	defer res.Body.Close()
	if res.IsError() {
		return output, status.Error(codes.FailedPrecondition, res.Status())
	}

	output.Count = 1
	output.Record = b
	return
}

// PutBatch by slice of given proto messages, when batch is processed, output only shows count
func (s *ESStorer) PutBatch(ctx context.Context, input *goxstore.Input) (output *goxstore.Output, err error) {
	output = &goxstore.Output{}
	if err = s.initSession(); err != nil {
		return output, err
	}

	var batchQuery strings.Builder
	for _, record := range input.Records {
		uid, err := getset.GetAsString(record, "uuid")
		if err != nil {
			return output, status.Error(codes.InvalidArgument, err.Error())
		}
		query := fmt.Sprintf(`{"index":{"_index":"%s","_type":"_doc","_id":"%s"}}`, cleanseIndex(input.Index), uid)
		if _, err = batchQuery.WriteString(query); err != nil {
			return output, status.Error(codes.InvalidArgument, err.Error())
		}
		if _, err = batchQuery.WriteString("\n"); err != nil {
			return output, status.Error(codes.InvalidArgument, err.Error())
		}
		m, err := opt.Marshal(record)
		if err != nil {
			return output, err
		}
		if _, err = batchQuery.Write(m); err != nil {
			return output, status.Error(codes.InvalidArgument, err.Error())
		}
		if _, err = batchQuery.WriteString("\n"); err != nil {
			return output, status.Error(codes.InvalidArgument, err.Error())
		}
	}

	res, err := s.esClient.Bulk(
		strings.NewReader(batchQuery.String()),
		s.esClient.Bulk.WithRefresh("true"),
		s.esClient.Bulk.WithErrorTrace(),
		s.esClient.Bulk.WithContext(ctx),
	)
	if err != nil {
		return output, status.Error(codes.FailedPrecondition, err.Error())
	}
	defer res.Body.Close()
	if res.IsError() {
		return output, status.Error(codes.FailedPrecondition, res.Status())
	}

	output.Count = len(input.Records)
	return
}

// Delete by uuid from proto message, output with single Record in bytes
func (s *ESStorer) Delete(ctx context.Context, input *goxstore.Input) (output *goxstore.Output, err error) {
	output = &goxstore.Output{}
	if err = s.initSession(); err != nil {
		return
	}
	uid, err := input.CheckUUID()
	if err != nil {
		return
	}
	output, err = s.Get(ctx, input)
	if err != nil {
		return
	}

	res, err := s.esClient.Delete(
		cleanseIndex(input.Index),
		uid,
		s.esClient.Delete.WithRefresh("true"),
		s.esClient.Delete.WithErrorTrace(),
		s.esClient.Delete.WithContext(ctx),
	)
	if err != nil {
		return output, status.Error(codes.FailedPrecondition, err.Error())
	}
	defer res.Body.Close()
	if res.IsError() {
		return output, status.Error(codes.FailedPrecondition, res.Status())
	}
	output.Count = 1

	return
}

// DeleteBatch by slice of given proto messages, when batch is processed, output only shows count
func (s *ESStorer) DeleteBatch(ctx context.Context, input *goxstore.Input) (output *goxstore.Output, err error) {
	output = &goxstore.Output{}
	if err = s.initSession(); err != nil {
		return
	}

	if len(input.Records) == 0 && input.Key == "" {
		// that would delete the whole index
		res, err := s.esClient.Indices.Delete(
			[]string{cleanseIndex(input.Index)},
			s.esClient.Indices.Delete.WithErrorTrace(),
			s.esClient.Indices.Delete.WithContext(ctx),
		)
		if err != nil {
			return output, status.Error(codes.FailedPrecondition, err.Error())
		}
		defer res.Body.Close()
		if res.IsError() {
			return output, status.Error(codes.FailedPrecondition, res.Status())
		}
		return output, nil
	}

	var batchQuery strings.Builder
	for _, record := range input.Records {
		uid, err := getset.GetAsString(record, "uuid")
		if err != nil {
			return output, status.Error(codes.InvalidArgument, err.Error())
		}
		query := fmt.Sprintf(`{"delete":{"_index":"%s","_type":"_doc","_id":"%s"}}`, cleanseIndex(input.Index), uid)
		if _, err = batchQuery.WriteString(query); err != nil {
			return output, status.Error(codes.InvalidArgument, err.Error())
		}
		if _, err = batchQuery.WriteString("\n"); err != nil {
			return output, status.Error(codes.InvalidArgument, err.Error())
		}
	}

	res, err := s.esClient.Bulk(
		strings.NewReader(batchQuery.String()),
		s.esClient.Bulk.WithRefresh("true"),
		s.esClient.Bulk.WithErrorTrace(),
		s.esClient.Bulk.WithContext(ctx),
	)
	if err != nil {
		return output, status.Error(codes.FailedPrecondition, err.Error())
	}
	defer res.Body.Close()
	if res.IsError() {
		return output, status.Error(codes.FailedPrecondition, res.Status())
	}
	output.Count = len(input.Records)

	return
}
