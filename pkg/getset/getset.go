// getset package is created to provide general type properties access
// simply use Get and Set on struct, map or protobuf message types
// it would underlyingly convert to json bytes and convert back to type
// only caveat is that json Number is usually a float, so may need casting
// if you want the getter value straight away
package getset

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"reflect"
	"strconv"
	"strings"

	"github.com/golang/protobuf/jsonpb"
	"github.com/golang/protobuf/proto"
	"github.com/tidwall/gjson"
	"github.com/tidwall/sjson"
)

func convertToMap(in interface{}) (bin []byte, err error) {
	if pin, ok := in.(proto.Message); ok {
		marshaler := new(jsonpb.Marshaler)
		marshaler.OrigName = true
		var bw []byte
		bout := bytes.NewBuffer(bw)
		err = marshaler.Marshal(bout, pin)
		if err != nil {
			return
		}
		bin = bout.Bytes()
		return
	}
	bin, err = json.Marshal(in)
	return
}

// Get return key in dot notation access to underlying value
// empty or non-existed value will return nil without error
// it only throws error when things go wrong
func Get(in interface{}, key string) (out interface{}, err error) {
	bin, err := convertToMap(in)
	if err != nil {
		return
	}
	result := gjson.GetBytes(bin, key)
	if !result.Exists() {
		out = nil
		return
	}
	if result.Type.String() == "Number" {
		resultStr := fmt.Sprintf("%v", result.Value())
		if strings.Contains(resultStr, ".") {
			out = result.Float()
		} else {
			out = result.Int()
		}
	} else if result.Type.String() == "JSON" {
		err = json.Unmarshal([]byte(result.Raw), &out)
		return
	}
	out = result.Value()
	return
}

// Set set value with key in dot notation access to underlying structure
// it only throws error when things go wrong
func Set(inPtr interface{}, key string, val interface{}) (err error) {
	if reflect.ValueOf(inPtr).Kind() != reflect.Ptr {
		return errors.New("accessor must be a pointer")
	}
	bin, err := convertToMap(inPtr)
	value, err := sjson.SetBytes(bin, key, val)
	if err != nil {
		return
	}
	if pin, ok := inPtr.(proto.Message); ok {
		err = jsonpb.Unmarshal(bytes.NewReader(value), pin)
		if err != nil {
			return
		}
		inPtr = &pin
		return
	}
	err = json.Unmarshal([]byte(value), &inPtr)
	return
}

// GetAsString is a helper function to type cast and convert getset interface result
func GetAsString(in interface{}, key string) (out string, err error) {
	var (
		i  interface{}
		ok bool
	)
	i, err = Get(in, key)
	if err != nil {
		return
	}
	if i == nil {
		return
	}
	out, ok = i.(string)
	if !ok {
		return "", errors.New("interface cannot cast as string")
	}
	return
}

// GetAsStrings is a helper function to type cast and convert getset interface result
// into slice of strings
func GetAsStrings(in interface{}, key string) (out []string, err error) {
	var (
		i interface{}
	)
	out = make([]string, 0)
	i, err = Get(in, key)
	if err != nil {
		return
	}
	if i == nil {
		return
	}
	ins, ok := i.([]interface{})
	if !ok {
		return out, errors.New("interface cannot cast as slice")
	}
	for _, each := range ins {
		item, ok := each.(string)
		if !ok {
			return out, errors.New("interface cannot cast as string")
		}
		out = append(out, item)
	}
	return
}

// GetAsBool is a helper function to type cast and convert getset interface result
func GetAsBool(in interface{}, key string) (out bool, err error) {
	var (
		i  interface{}
		ok bool
	)
	i, err = Get(in, key)
	if err != nil {
		return
	}
	if i == nil {
		return
	}
	out, ok = i.(bool)
	if !ok {
		return false, errors.New("interface cannot cast as boolean")
	}
	return
}

// GetAsInt is a helper function to type cast and convert getset interface result
func GetAsInt(in interface{}, key string) (out int, err error) {
	var (
		i  interface{}
		ok bool
	)
	i, err = Get(in, key)
	if err != nil {
		return
	}
	out, ok = i.(int)
	if !ok {
		return 0, errors.New("interface cannot cast as int")
	}
	return
}

// GetAsInt32 is a helper function to type cast and convert getset int32erface result
func GetAsInt32(in interface{}, key string) (out int32, err error) {
	var (
		i  interface{}
		ok bool
	)
	i, err = Get(in, key)
	if err != nil {
		return
	}
	out, ok = i.(int32)
	if !ok {
		return 0, errors.New("interface cannot cast as int32")
	}
	return
}

// GetAsInt64 is a helper function to type cast and convert getset int64erface result
func GetAsInt64(in interface{}, key string) (out int64, err error) {
	var (
		i  interface{}
		ok bool
	)
	i, err = Get(in, key)
	if err != nil {
		return
	}
	out, ok = i.(int64)
	if !ok {
		sOut, ok := i.(string)
		if !ok {
			return 0, errors.New("interface cannot cast as int64")
		}
		if out, err = strconv.ParseInt(sOut, 10, 64); err != nil {
			return 0, errors.New("interface cannot cast as int64")
		}
	}
	return
}

// GetAsFloat32 is a helper function to type cast and convert getset interface result
func GetAsFloat32(in interface{}, key string) (out float32, err error) {
	var (
		i  interface{}
		ok bool
	)
	i, err = Get(in, key)
	if err != nil {
		return
	}
	out, ok = i.(float32)
	if !ok {
		return 0, errors.New("interface cannot cast as float32")
	}
	return
}

// GetAsFloat64 is a helper function to type cast and convert getset interface result
func GetAsFloat64(in interface{}, key string) (out float64, err error) {
	var (
		i  interface{}
		ok bool
	)
	i, err = Get(in, key)
	if err != nil {
		return
	}
	out, ok = i.(float64)
	if !ok {
		return 0, errors.New("interface cannot cast as float64")
	}
	return
}
