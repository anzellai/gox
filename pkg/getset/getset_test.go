package getset

import (
	"bytes"
	"encoding/json"
	"fmt"
	"testing"

	"github.com/golang/protobuf/jsonpb"
	structpb "github.com/golang/protobuf/ptypes/struct"
)

func TestGetSetWithStruct(t *testing.T) {
	s := struct {
		FieldA string
		FieldB int64
		FieldC []string
	}{
		FieldA: "FieldA",
		FieldB: 1,
		FieldC: []string{"a", "b"},
	}

	fieldA, err := Get(s, "FieldA")
	if err != nil || fieldA != s.FieldA {
		t.Fatalf("Get error or values not equal: %v - %v", s, err)
	}
	fieldB, err := Get(s, "FieldB")
	if err != nil || fmt.Sprintf("%v", fieldB) != fmt.Sprintf("%v", s.FieldB) {
		t.Fatalf("Get error or values not equal: %v - %v", s, err)
	}
	fieldC, err := Get(s, "FieldC.1")
	if err != nil || fieldC != s.FieldC[1] {
		t.Fatalf("Get error or values not equal: %v - %v", s, err)
	}
	fieldCStrings, err := GetAsStrings(s, "FieldC")
	if err != nil || fmt.Sprintf("%v", fieldCStrings) != fmt.Sprintf("%v", s.FieldC) {
		t.Fatalf("Get error or values not equal: %v - %v", s, err)
	}

	err = Set(&s, "FieldA", "fieldA")
	if err != nil || s.FieldA != "fieldA" {
		t.Fatalf("Set error or values not equal: %v - %v", s, err)
	}
	err = Set(&s, "FieldB", 2)
	if err != nil || s.FieldB != 2 {
		t.Fatalf("Set error or values not equal: %v - %v", s, err)
	}
	err = Set(&s, "FieldC.1", "c")
	if err != nil || s.FieldC[1] != "c" {
		t.Fatalf("Set error or values not equal: %v - %v", s, err)
	}
	err = Set(&s, "FieldC", []string{"c"})
	if err != nil || len(s.FieldC) != 1 || s.FieldC[0] != "c" {
		t.Fatalf("Set error or values not equal: %v - %v", s, err)
	}
}

func TestGetSetWithMap(t *testing.T) {
	a, b := "a", "b"
	s := map[string]interface{}{
		"FieldA": "FieldA",
		"FieldB": 1,
		"FieldC": []string{a, b},
	}

	fieldA, err := Get(s, "FieldA")
	if err != nil || fieldA != s["FieldA"] {
		t.Fatalf("Get error or values not equal: %v - %v", s, err)
	}
	fieldB, err := Get(s, "FieldB")
	if err != nil || fmt.Sprintf("%v", fieldB) != fmt.Sprintf("%v", s["FieldB"]) {
		t.Fatalf("Get error or values not equal: %v - %v", s, err)
	}
	fieldC, err := Get(s, "FieldC.1")
	if err != nil || fieldC != b {
		t.Fatalf("Get error or values not equal: %v - %v", s, err)
	}
	fieldCStrings, err := GetAsStrings(s, "FieldC")
	if err != nil || fmt.Sprintf("%v", fieldCStrings) != fmt.Sprintf("%v", s["FieldC"]) {
		t.Fatalf("Get error or values not equal: %v - %v", s, err)
	}

	err = Set(&s, "FieldA", "fieldA")
	if err != nil || s["FieldA"] != "fieldA" {
		t.Fatalf("Set error or values not equal: %v - %v", s, err)
	}
	err = Set(&s, "FieldB", 2)
	if err != nil || fmt.Sprintf("%v", s["FieldB"]) != fmt.Sprintf("%v", 2) {
		t.Fatalf("Set error or values not equal: %v - %v", s, err)
	}
	err = Set(&s, "FieldC.1", "c")
	if err != nil || s["FieldC"].([]interface{})[1].(string) != "c" {
		t.Fatalf("Set error or values not equal: %v - %v", s, err)
	}

	err = Set(&s, "FieldC", []string{"c"})
	fieldC, err = Get(s, "FieldC.0")
	if err != nil || fieldC != "c" {
		t.Fatalf("Get error or values not equal: %v - %v", s, err)
	}
}

func TestGetSetWithProto(t *testing.T) {
	a, b := "a", "b"
	s := map[string]interface{}{
		"stringField":  "FieldA",
		"intField":     1,
		"stringsField": []string{a, b},
		"mapField":     map[string]string{"keyA": "valA"},
	}
	bs, err := json.Marshal(s)
	if err != nil {
		t.Fatalf("json.Marshal error: %v", err)
	}
	msg := &structpb.Struct{}
	if err = jsonpb.Unmarshal(bytes.NewReader(bs), msg); err != nil {
		t.Fatalf("jsonpb.Unmarshal error: %v", err)
	}
	sf, err := Get(msg, "stringField")
	if err != nil || fmt.Sprintf("%v", sf) != fmt.Sprintf("%v", s["stringField"]) {
		t.Fatalf("Get error or values not equal: %v - %v", msg, err)
	}
	sf1, err := Get(msg, "stringsField.1")
	if err != nil || fmt.Sprintf("%v", sf1) != fmt.Sprintf("%v", b) {
		t.Fatalf("Get error or values not equal: %v - %v", sf1, err)
	}
	mfa, err := Get(msg, "mapField.keyA")
	if err != nil || fmt.Sprintf("%v", mfa) != fmt.Sprintf("%v", "valA") {
		t.Fatalf("Get error or values not equal: %v - %v", mfa, err)
	}

	err1 := Set(msg, "stringField", "FieldB")
	err2 := Set(msg, "stringsFiled.1", 3)
	err3 := Set(msg, "stringsField", []uint64{3})
	err4 := Set(msg, "mapField.keyA", "valB")
	if err1 != nil || err2 != nil || err3 != nil || err4 != nil {
		t.Fatalf("Set error from previous steps: %v %v %v %v", err1, err2, err3, err4)
	}
	mb := make(map[string]interface{})
	marshaler := new(jsonpb.Marshaler)
	ms, err := marshaler.MarshalToString(msg)
	if err != nil {
		t.Fatalf("jsonpb.Marshal error: %v", err)
	}
	if err = json.Unmarshal([]byte(ms), &mb); err != nil {
		t.Fatalf("json.Unmarshal error: %v", err)
	}
	if fmt.Sprintf("%v", mb["stringField"]) != fmt.Sprintf("%v", "FieldB") {
		t.Fatalf("Set error or values not equal: %v - %v", mb, err)
	}
	if err != nil || fmt.Sprintf("%v", mb["stringsField"]) != fmt.Sprintf("%v", []uint64{3}) {
		t.Fatalf("Set error or values not equal: %v - %v", mb, err)
	}
	if err != nil || fmt.Sprintf("%v", mb["mapField"]) != fmt.Sprintf("%v", map[string]string{"keyA": "valB"}) {
		t.Fatalf("Set error or values not equal: %v - %v", mb, err)
	}
}
