# getset

## Summary

getset package is created to provide general type properties access
simply use Get and Set on struct, map or protobuf message types
it would underlyingly convert to json bytes and convert back to type
only caveat is that json Number is usually a float, so may need casting
if you want the getter value straight away

