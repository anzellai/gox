package protomsg

import (
	"bytes"
	"encoding/json"
	"errors"
	"reflect"

	"github.com/golang/protobuf/jsonpb"
	"github.com/golang/protobuf/proto"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// Option type definition for use with proto.Message and interface
// empty values are used as default
type Option struct {
	MarshalDefaults   bool // same as jsonpb.Marshaler EmitDefaults
	MarshalCamelCase  bool // opposite of jsonpb.Marshaler OrigName
	UnmarshalerStrict bool // opposite of jsonpb.Unmarshaler AllowUnknownFields
}

var DefaultOption *Option = new(Option)

// Marshal method to marshal either proto.Message or interface types
func (o *Option) Marshal(in interface{}) ([]byte, error) {
	switch v := in.(type) {
	case []proto.Message:
		marshaler := new(jsonpb.Marshaler)
		marshaler.OrigName = true
		if o.MarshalDefaults {
			marshaler.EmitDefaults = true
		}
		if o.MarshalCamelCase {
			marshaler.OrigName = false
		}
		var b []byte
		bb := bytes.NewBuffer(b)
		bb.Write([]byte(`[`))
		for _, pm := range in.([]proto.Message) {
			if err := marshaler.Marshal(bb, pm); err != nil {
				return nil, status.Error(codes.DataLoss, err.Error())
			}
		}
		bb.Write([]byte(`]`))
		return bb.Bytes(), nil

	case proto.Message:
		marshaler := new(jsonpb.Marshaler)
		marshaler.OrigName = true
		if o.MarshalDefaults {
			marshaler.EmitDefaults = true
		}
		if o.MarshalCamelCase {
			marshaler.OrigName = false
		}
		vs, err := marshaler.MarshalToString(v)
		if err != nil {
			return nil, status.Error(codes.DataLoss, err.Error())
		}
		return []byte(vs), nil
	}

	out, err := json.Marshal(in)
	if err != nil {
		return nil, status.Error(codes.DataLoss, err.Error())
	}
	return out, nil
}

// UnmarshalNext return channel of iterator to save memory footprint
// results could be unordered
func (o *Option) UnmarshalNext(in interface{}) (<-chan []byte, error) {
	var (
		bIn []byte
		err error
	)
	switch i := in.(type) {
	case []byte:
		bIn = i

	default:
		bIn, err = o.Marshal(in)
		if err != nil {
			return nil, err
		}
	}

	msgChan := make(chan []byte)
	decoder := json.NewDecoder(bytes.NewReader(bIn))
	if _, err = decoder.Token(); err != nil {
		return nil, status.Errorf(codes.DataLoss, err.Error())
	}

	go func() {
		for decoder.More() {
			i := make(map[string]interface{})
			if err := decoder.Decode(&i); err != nil {
				msgChan <- nil
				panic(status.Errorf(codes.DataLoss, err.Error()))
			}
			b, err := o.Marshal(i)
			if err != nil {
				msgChan <- nil
				panic(err)
			}
			msgChan <- b
		}
		close(msgChan)
	}()

	return msgChan, err
}

// UnmarshalBytes return slice of bytes from complete marshalled input
// results are ordered
func (o *Option) UnmarshalBytes(in interface{}) ([][]byte, error) {
	var (
		bIn []byte
		err error
	)
	switch i := in.(type) {
	case []byte:
		bIn = i

	default:
		bIn, err = o.Marshal(in)
		if err != nil {
			return nil, err
		}
	}

	msgs := make([][]byte, 0)
	decoder := json.NewDecoder(bytes.NewReader(bIn))
	if _, err = decoder.Token(); err != nil {
		return nil, status.Errorf(codes.DataLoss, err.Error())
	}

	for decoder.More() {
		i := make(map[string]interface{})
		if err := decoder.Decode(&i); err != nil {
			return nil, status.Errorf(codes.DataLoss, err.Error())
		}
		b, err := o.Marshal(i)
		if err != nil {
			return nil, err
		}
		msgs = append(msgs, b)
	}

	return msgs, nil
}

// Unmarshal method to unmarshal from proto.Message, []byte or interface types
// into either proto.Message or interface types
func (o *Option) Unmarshal(in interface{}, out interface{}) error {
	var (
		bIn []byte
		err error
	)
	switch i := in.(type) {
	case []byte:
		bIn = i

	default:
		bIn, err = o.Marshal(in)
		if err != nil {
			return err
		}
	}

	switch v := out.(type) {
	case []proto.Message:
		err := errors.New("unsupported dst of []proto.Message, use UnmarshalNext instead")
		return status.Error(codes.InvalidArgument, err.Error())

	case proto.Message:
		unmarshaler := new(jsonpb.Unmarshaler)
		unmarshaler.AllowUnknownFields = true
		if o.UnmarshalerStrict {
			unmarshaler.AllowUnknownFields = false
		}
		if err := unmarshaler.Unmarshal(bytes.NewReader(bIn), v); err != nil {
			return status.Error(codes.DataLoss, err.Error())
		}
		return nil
	}

	if err := json.Unmarshal(bIn, out); err != nil {
		return status.Error(codes.DataLoss, err.Error())
	}
	return nil
}

// Clone returns a deep copy of src.
func Clone(in interface{}) interface{} {
	switch v := in.(type) {
	case proto.Message:
		return proto.Clone(v)
	}
	var out interface{}
	if err := DefaultOption.Unmarshal(in, &out); err != nil {
		panic(err)
	}
	return out
}

//Merge merges src into dst, which must be messages of the same type.
func Merge(dst interface{}, src interface{}) {
	var (
		dIn, sIn []byte
		err      error
	)
	dIn, err = DefaultOption.Marshal(dst)
	if err != nil {
		panic(err)
	}
	sIn, err = DefaultOption.Marshal(src)
	if err != nil {
		panic(err)
	}

	var m map[string]interface{}
	if err = DefaultOption.Unmarshal(dIn, &m); err != nil {
		panic(err)
	}
	if err = DefaultOption.Unmarshal(sIn, &m); err != nil {
		panic(err)
	}

	b, err := DefaultOption.Marshal(m)
	if err != nil {
		panic(err)
	}

	if err = DefaultOption.Unmarshal(b, dst); err != nil {
		panic(err)
	}
}

// BindOne converts raw bytes into single proto.Message and parse to given dst
func BindOne(record []byte, dst proto.Message) error {
	err := DefaultOption.Unmarshal(record, dst)
	if err != nil {
		return status.Error(codes.DataLoss, err.Error())
	}
	return nil
}

// BindMulti converts raw bytes into range of dst parser from given base type
func BindMulti(records []byte) (<-chan []byte, error) {
	return DefaultOption.UnmarshalNext(records)
}

// BindAll converts raw bytes into range of dst parser from given base type
func BindAll(records []byte) ([][]byte, error) {
	return DefaultOption.UnmarshalBytes(records)
}

// Diff to compare 2 objects and returned a map with fields of different values
// always compared objA to objB, missing fields in objB will be merged into diff as nil
// objA and objB can be either map, protobuf message or non-array json marshalling types
// Diff does not go deeper than base level as nested changes are intended to pass as whole value
// Do not care about code repetition, it is better than having another function call
func Diff(objA, objB interface{}) (diff map[string]interface{}, err error) {
	var (
		mapA, mapB map[string]interface{}
	)

	diff = map[string]interface{}{}
	protoA, ok := objA.(proto.Message)
	if ok {
		b, err := DefaultOption.Marshal(protoA)
		if err != nil {
			return nil, err
		}
		err = DefaultOption.Unmarshal(b, &mapA)
		if err != nil {
			return nil, status.Error(codes.DataLoss, err.Error())
		}
	} else if reflect.TypeOf(objA).Kind() == reflect.Slice {
		return nil, status.Error(codes.InvalidArgument, codes.InvalidArgument.String())
	} else {
		b, err := DefaultOption.Marshal(objA)
		if err != nil {
			return nil, err
		}
		err = DefaultOption.Unmarshal(b, &mapA)
		if err != nil {
			return nil, err
		}
	}

	protoB, ok := objB.(proto.Message)
	if ok {
		b, err := DefaultOption.Marshal(protoB)
		if err != nil {
			return nil, err
		}
		err = DefaultOption.Unmarshal(b, &mapB)
		if err != nil {
			return nil, status.Error(codes.DataLoss, err.Error())
		}
	} else if reflect.TypeOf(objB).Kind() == reflect.Slice {
		return nil, status.Error(codes.InvalidArgument, codes.InvalidArgument.String())
	} else {
		b, err := DefaultOption.Marshal(objB)
		if err != nil {
			return nil, err
		}
		err = DefaultOption.Unmarshal(b, &mapB)
		if err != nil {
			return nil, err
		}
	}

	// simply compare from mapA to mapB by mapA key/value to mapB
	for keyA, valA := range mapA {
		valB, ok := mapB[keyA]
		if !ok {
			diff[keyA] = valA
			continue
		}
		jsonA, err := DefaultOption.Marshal(valA)
		if err != nil {
			return nil, err
		}
		jsonB, err := DefaultOption.Marshal(valB)
		if err != nil {
			return nil, err
		}
		if string(jsonA) == string(jsonB) {
			continue
		}
		diff[keyA] = valA
	}
	// any missing field from mapB will be updated in diff map as empty value
	for keyB, valB := range mapB {
		_, ok := mapA[keyB]
		if !ok {
			var v interface{}
			switch reflect.TypeOf(valB).Kind() {
			case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
				v = 0
			case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
				v = 0
			case reflect.Float32, reflect.Float64:
				v = 0
			case reflect.Bool:
				v = false
			case reflect.String:
				v = ""
			case reflect.Chan, reflect.Func, reflect.Interface, reflect.Ptr:
				if reflect.ValueOf(valB).IsNil() {
					v = reflect.Zero(reflect.TypeOf(valB))
				} else {
					v = nil
				}
			case reflect.Map:
				v = make(map[string]interface{}, 0)
			case reflect.Slice:
				v = make([]interface{}, 0)
			}
			diff[keyB] = v
		}
	}
	return
}
