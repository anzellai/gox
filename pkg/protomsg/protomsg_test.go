package protomsg

import (
	"bytes"
	"os"
	"testing"
	"time"

	"bitbucket.org/anzellai/gox/cmd/example/proto/v1ExampleAPI"
	"google.golang.org/protobuf/types/known/timestamppb"
	"google.golang.org/protobuf/types/known/wrapperspb"
)

func BenchmarkMarshal(b *testing.B) {
	env := os.Getenv("ENV")
	os.Setenv("ENV", "TEST")
	defer os.Setenv("ENV", env)

	example := &v1ExampleAPI.Task{
		Uuid:      "test-uuid",
		Title:     "test title",
		Completed: &wrapperspb.BoolValue{Value: true},
		CreatedAt: &timestamppb.Timestamp{Seconds: time.Now().UTC().Unix()},
	}

	examples := make([]*v1ExampleAPI.Task, 0)
	for n := 0; n < 10000; n++ {
		examples = append(examples, example)
	}

	opt := new(Option)

	b.ResetTimer()
	b.Run("benchmarking single protomsg.Marshal", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			for n := 0; n < 10000; n++ {
				opt.Marshal(example)
			}
		}
	})

	b.Run("benchmarking bulk protomsg.Marshal", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			opt.Marshal(examples)
		}
	})
}

func BenchmarkUnmarshal(b *testing.B) {
	env := os.Getenv("ENV")
	os.Setenv("ENV", "TEST")
	defer os.Setenv("ENV", env)

	example := &v1ExampleAPI.Task{
		Uuid:      "test-uuid",
		Title:     "test title",
		Completed: &wrapperspb.BoolValue{Value: true},
		CreatedAt: &timestamppb.Timestamp{Seconds: time.Now().UTC().Unix()},
	}

	examples := make([]*v1ExampleAPI.Task, 0)
	for n := 0; n < 10000; n++ {
		examples = append(examples, example)
	}

	opt := new(Option)

	bExample, _ := opt.Marshal(example)
	bExamples, _ := opt.Marshal(examples)

	b.ResetTimer()
	b.Run("benchmarking single protomsg.Unmarshal", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			pms := make([]*v1ExampleAPI.Task, 0)
			for n := 0; n < 10000; n++ {
				pm := &v1ExampleAPI.Task{}
				opt.Unmarshal(bytes.NewReader(bExample), pm)
				pms = append(pms, pm)
			}
		}
	})

	b.Run("benchmarking bulk protomsg.Marshal", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			mms := make([]map[string]interface{}, 0)
			pms := make([]*v1ExampleAPI.Task, 0)
			opt.Unmarshal(bExamples, &mms)
			for _, mm := range mms {
				pm := &v1ExampleAPI.Task{}
				bPM, _ := opt.Marshal(mm)
				opt.Unmarshal(bytes.NewReader(bPM), pm)
				pms = append(pms, pm)
			}
		}
	})
}
