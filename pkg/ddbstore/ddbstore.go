package ddbstore

import (
	"context"
	"os"
	"time"

	"bitbucket.org/anzellai/gox/pkg/getset"
	"bitbucket.org/anzellai/gox/pkg/goxstore"
	"bitbucket.org/anzellai/gox/pkg/protomsg"
	"bitbucket.org/anzellai/gox/pkg/pbsession"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var (
	opt = protomsg.DefaultOption
)

// DDBStorer implements AWS DynamoDB CRUD and Key/Value access APIs
type DDBStorer struct {
	sess      *session.Session
	ddbClient *dynamodb.DynamoDB
}

// New return DDBStorer pointer
func New() *DDBStorer {
	return &DDBStorer{}
}

// Close clear up unused session
func (s *DDBStorer) Close() error {
	s.ddbClient = nil
	s.sess = nil
	return nil
}

// SetSession override default AWS Session
func (s *DDBStorer) SetSession(sess *session.Session) *DDBStorer {
	s.sess = sess
	return s
}

// SetClient override default DDB Client
func (s *DDBStorer) SetClient(client *dynamodb.DynamoDB) *DDBStorer {
	s.ddbClient = client
	return s
}

// initSession to ensure DDBStorer has AWS session initialised
func (s *DDBStorer) initSession() error {
	if s.sess == nil {
		sess, err := pbsession.New()
		if err != nil {
			return status.Error(codes.FailedPrecondition, err.Error())
		}
		s.sess = sess
	}
	if s.ddbClient == nil {
		if os.Getenv("USE_LOCALSTACK") == "1" {
			s.ddbClient = dynamodb.New(s.sess, &aws.Config{
				Endpoint: aws.String(os.Getenv("LOCALSTACK_URL")),
			})
		} else {
			s.ddbClient = dynamodb.New(s.sess)
		}
	}
	return nil
}

// Info as in optional health check info operation
func (s *DDBStorer) Info(ctx context.Context, input *goxstore.Input) (output *goxstore.Output, err error) {
	output = &goxstore.Output{}
	if err = s.initSession(); err != nil {
		return
	}
	// if no input or index is given, just ping the endpoint api
	if input == nil || input.Index == "" {
		endpointOutput, err := s.ddbClient.DescribeEndpoints(&dynamodb.DescribeEndpointsInput{})
		if err != nil {
			return output, status.Error(codes.FailedPrecondition, err.Error())
		}
		return &goxstore.Output{Info: []byte(endpointOutput.String())}, nil
	}
	// if input index is given ping the table and return its table info
	ddbOutput, err := s.ddbClient.DescribeTable(&dynamodb.DescribeTableInput{TableName: aws.String(input.Index)})
	if err != nil {
		return output, status.Error(codes.FailedPrecondition, err.Error())
	}
	b, err := opt.Marshal(ddbOutput)
	if err != nil {
		return output, status.Error(codes.DataLoss, err.Error())
	}

	return &goxstore.Output{Info: b}, nil
}

// Get by uuid from proto message, output with single Record in bytes
func (s *DDBStorer) Get(ctx context.Context, input *goxstore.Input) (output *goxstore.Output, err error) {
	output = &goxstore.Output{}
	if err = s.initSession(); err != nil {
		return
	}
	uid, err := input.CheckUUID()
	if err != nil {
		return
	}

	ddbInput := &dynamodb.GetItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"uuid": {
				S: aws.String(uid),
			},
		},
		TableName: aws.String(input.Index),
	}
	ddbOutput, err := s.ddbClient.GetItem(ddbInput)
	if err != nil {
		return output, status.Error(codes.FailedPrecondition, err.Error())
	}
	if len(ddbOutput.Item) == 0 {
		return output, status.Error(codes.NotFound, codes.NotFound.String())
	}
	mOut := make(map[string]interface{})
	if err = dynamodbattribute.UnmarshalMap(ddbOutput.Item, &mOut); err != nil {
		return output, status.Error(codes.DataLoss, err.Error())
	}
	if input.TTL {
		now := time.Now().UTC().Format(time.RFC3339)
		ttl, err := getset.GetAsString(mOut, "ttl")
		if err != nil {
			return output, status.Error(codes.DataLoss, err.Error())
		}
		if ttl != "" && ttl < now {
			return output, status.Error(codes.NotFound, codes.NotFound.String())
		}
	}

	b, err := opt.Marshal(mOut)
	if err != nil {
		return output, status.Error(codes.DataLoss, err.Error())
	}

	output.Record = b
	output.Count = 1
	return
}

// Query by uuid from proto message, output with slice of Records in bytes
func (s *DDBStorer) Query(ctx context.Context, input *goxstore.Input) (output *goxstore.Output, err error) {
	output = &goxstore.Output{}
	if err = s.initSession(); err != nil {
		return
	}

	if input == nil || input.Query == nil {
		err = status.Error(codes.InvalidArgument, codes.Internal.String())
		return
	}

	scanInput := &dynamodb.ScanInput{
		TableName: &input.Index,
	}
	if input.Query.After.Value != nil {
		after, ok := input.Query.After.Value.(map[string]*dynamodb.AttributeValue)
		if !ok {
			err = status.Error(codes.InvalidArgument, codes.InvalidArgument.String())
			return
		}
		scanInput.SetExclusiveStartKey(after)
	}
	scanned, err := s.ddbClient.ScanWithContext(ctx, scanInput)
	if err != nil {
		err = status.Errorf(codes.FailedPrecondition, err.Error())
		return
	}

	outs := make([]map[string]interface{}, 0)
	maps := make([]map[string]interface{}, 0)

	if err = dynamodbattribute.UnmarshalListOfMaps(scanned.Items, &maps); err != nil {
		err = status.Errorf(codes.DataLoss, err.Error())
		return
	}
	outs = append(outs, maps...)

	for {
		maps = make([]map[string]interface{}, 0)
		if len(scanned.LastEvaluatedKey) > 0 {
			scanInput.SetExclusiveStartKey(scanned.LastEvaluatedKey)
			scanned, err = s.ddbClient.ScanWithContext(ctx, scanInput)
			if err != nil {
				err = status.Errorf(codes.FailedPrecondition, err.Error())
				return
			}
			if err = dynamodbattribute.UnmarshalListOfMaps(scanned.Items, &maps); err != nil {
				err = status.Errorf(codes.DataLoss, err.Error())
				return
			}
			outs = append(outs, maps...)
			continue
		}
		break
	}

	maps = nil
	outs = input.Query.Eval(outs)
	b, err := opt.Marshal(outs)
	if err != nil {
		return
	}

	output.Records = b
	output.Count = len(outs)

	return
}

// Put by uuid to upsert given proto message, output with slice of Records in bytes
func (s *DDBStorer) Put(ctx context.Context, input *goxstore.Input) (output *goxstore.Output, err error) {
	output = &goxstore.Output{}
	if err = s.initSession(); err != nil {
		return
	}

	o := protomsg.Clone(input.Record)
	output, err = s.Get(ctx, input)
	if err != nil {
		if status.Code(err) != codes.NotFound {
			return
		}
	} else {
		if err = opt.Unmarshal(output.Record, o); err != nil {
			return
		}
		protomsg.Merge(o, input.Record)
	}
	mIn := make(map[string]interface{})
	if err = opt.Unmarshal(o, &mIn); err != nil {
		return
	}
	attrs, err := dynamodbattribute.MarshalMap(mIn)
	if err != nil {
		return output, status.Error(codes.DataLoss, err.Error())
	}
	ddbInput := &dynamodb.PutItemInput{
		Item:      attrs,
		TableName: aws.String(input.Index),
	}
	_, err = s.ddbClient.PutItem(ddbInput)
	if err != nil {
		return output, status.Error(codes.FailedPrecondition, err.Error())
	}
	b, err := opt.Marshal(o)
	if err != nil {
		return
	}

	output.Record = b
	output.Count = 1
	return
}

// PutBatch by slice of given proto messages, when batch is processed, output only shows count
// This is not atmoic, consideration should be taken when performing batch and failure or retry is needed
// NOTE: provided proto messages must be FULL message as it overwrites existing record
func (s *DDBStorer) PutBatch(ctx context.Context, input *goxstore.Input) (output *goxstore.Output, err error) {
	output = &goxstore.Output{}
	if err = s.initSession(); err != nil {
		return
	}

	ddbInputs := make([]*dynamodb.WriteRequest, 0)
	duplicatedKeys := make(map[string]bool)
	for _, record := range input.Records {
		mIn := make(map[string]interface{})
		if err := opt.Unmarshal(record, &mIn); err != nil {
			return output, err
		}
		recordInput := &goxstore.Input{Index: input.Index, Record: record}
		uid, err := recordInput.CheckUUID()
		if err != nil {
			return output, err
		}
		attrs, err := dynamodbattribute.MarshalMap(mIn)
		if err != nil {
			return output, status.Error(codes.DataLoss, err.Error())
		}
		ddbInput := &dynamodb.WriteRequest{
			PutRequest: &dynamodb.PutRequest{
				Item: attrs,
			},
		}
		// NOTE: dynamodb will throw Validation exception:
		// Provided list of item keys contains duplicates error
		// on patch PUT calls
		if _, duplicated := duplicatedKeys[uid]; duplicated {
			batchInput := &dynamodb.BatchWriteItemInput{}
			batchInput.SetRequestItems(map[string][]*dynamodb.WriteRequest{
				input.Index: ddbInputs,
			})
			_, err := s.ddbClient.BatchWriteItem(batchInput)
			if err != nil {
				return output, status.Error(codes.FailedPrecondition, err.Error())
			}
			output.Count += len(ddbInputs)
			ddbInputs = make([]*dynamodb.WriteRequest, 0)
			duplicatedKeys = make(map[string]bool)
			continue
		}

		duplicatedKeys[uid] = true
		ddbInputs = append(ddbInputs, ddbInput)
		if len(ddbInputs) >= 25 {
			batchInput := &dynamodb.BatchWriteItemInput{}
			batchInput.SetRequestItems(map[string][]*dynamodb.WriteRequest{
				input.Index: ddbInputs,
			})
			_, err := s.ddbClient.BatchWriteItem(batchInput)
			if err != nil {
				return output, status.Error(codes.FailedPrecondition, err.Error())
			}
			output.Count += len(ddbInputs)
			ddbInputs = make([]*dynamodb.WriteRequest, 0)
		}
	}
	if len(ddbInputs) > 0 {
		batchInput := &dynamodb.BatchWriteItemInput{}
		batchInput.SetRequestItems(map[string][]*dynamodb.WriteRequest{
			input.Index: ddbInputs,
		})
		_, err := s.ddbClient.BatchWriteItem(batchInput)
		if err != nil {
			return output, status.Error(codes.FailedPrecondition, err.Error())
		}
		output.Count += len(ddbInputs)
		ddbInputs = nil
	}

	return
}

// Delete by uuid from proto message, output with single Record in bytes
func (s *DDBStorer) Delete(ctx context.Context, input *goxstore.Input) (output *goxstore.Output, err error) {
	output = &goxstore.Output{}
	if err = s.initSession(); err != nil {
		return
	}
	uid, err := input.CheckUUID()
	if err != nil {
		return
	}

	output, err = s.Get(ctx, input)
	if err != nil {
		if status.Code(err) != codes.NotFound {
			return
		}
	}
	ddbInput := &dynamodb.DeleteItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"uuid": {
				S: aws.String(uid),
			},
		},
		TableName: aws.String(input.Index),
	}
	_, err = s.ddbClient.DeleteItem(ddbInput)
	if err != nil {
		return output, status.Error(codes.FailedPrecondition, err.Error())
	}

	return
}

// DeleteBatch by slice of given proto messages, when batch is processed, output only shows count
// This is not atmoic, consideration should be taken when performing batch and failure or retry is needed
func (s *DDBStorer) DeleteBatch(ctx context.Context, input *goxstore.Input) (output *goxstore.Output, err error) {
	if err = s.initSession(); err != nil {
		return
	}

	output = &goxstore.Output{}
	ddbInputs := make([]*dynamodb.WriteRequest, 0)
	duplicatedKeys := make(map[string]bool)
	for _, record := range input.Records {
		mIn := make(map[string]interface{})
		if err := opt.Unmarshal(record, &mIn); err != nil {
			return output, err
		}
		recordInput := &goxstore.Input{Index: input.Index, Record: record}
		uid, err := recordInput.CheckUUID()
		if err != nil {
			return output, err
		}
		ddbInput := &dynamodb.WriteRequest{
			DeleteRequest: &dynamodb.DeleteRequest{
				Key: map[string]*dynamodb.AttributeValue{
					"uuid": {
						S: aws.String(uid),
					},
				},
			},
		}
		// NOTE: dynamodb will throw Validation exception:
		// Provided list of item keys contains duplicates error
		// on patch PUT calls
		if _, duplicated := duplicatedKeys[uid]; duplicated {
			batchInput := &dynamodb.BatchWriteItemInput{}
			batchInput.SetRequestItems(map[string][]*dynamodb.WriteRequest{
				input.Index: ddbInputs,
			})
			_, err := s.ddbClient.BatchWriteItem(batchInput)
			if err != nil {
				return output, status.Error(codes.FailedPrecondition, err.Error())
			}
			output.Count += len(ddbInputs)
			ddbInputs = make([]*dynamodb.WriteRequest, 0)
			duplicatedKeys = make(map[string]bool)
			continue
		}

		duplicatedKeys[uid] = true
		ddbInputs = append(ddbInputs, ddbInput)
		if len(ddbInputs) >= 25 {
			batchInput := &dynamodb.BatchWriteItemInput{}
			batchInput.SetRequestItems(map[string][]*dynamodb.WriteRequest{
				input.Index: ddbInputs,
			})
			_, err := s.ddbClient.BatchWriteItem(batchInput)
			if err != nil {
				return output, status.Error(codes.FailedPrecondition, err.Error())
			}
			output.Count += len(ddbInputs)
			ddbInputs = make([]*dynamodb.WriteRequest, 0)
		}
	}
	if len(ddbInputs) > 0 {
		batchInput := &dynamodb.BatchWriteItemInput{}
		batchInput.SetRequestItems(map[string][]*dynamodb.WriteRequest{
			input.Index: ddbInputs,
		})
		_, err := s.ddbClient.BatchWriteItem(batchInput)
		if err != nil {
			return output, status.Error(codes.FailedPrecondition, err.Error())
		}
		output.Count += len(ddbInputs)
		ddbInputs = nil
	}

	return
}
