package goxlogger

import (
	"context"
	"os"

	"github.com/grpc-ecosystem/go-grpc-middleware/logging/zap/ctxzap"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// Logger type definition
type Logger struct {
	logger      *zap.Logger
	errorLogger bool
}

// Fields shorthand type definition
type Fields map[string]interface{}

// GetLogger return a logger extracted from context or a new logger
func GetLogger(ctx context.Context) (context.Context, *Logger) {
	logger := ctxzap.Extract(ctx)
	if logger == nil {
		logger = NewLogger().logger
	}
	newCtx := ctxzap.ToContext(ctx, logger)
	defer logger.Sync()
	return newCtx, &Logger{logger: logger}
}

// CtxLogger return a context has logger value
func CtxLogger(ctx context.Context, logger *Logger) context.Context {
	return ctxzap.ToContext(ctx, NewLogger().logger)
}

// NewLogger return an initialised Logger
func NewLogger() *Logger {
	conf := zap.NewProductionConfig()
	conf.EncoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder
	if os.Getenv("ENV") == "TEST" {
		conf.OutputPaths = []string{"/dev/null"}
		conf.ErrorOutputPaths = []string{"/dev/null"}
	} else {
		conf.OutputPaths = []string{"stdout"}
		conf.ErrorOutputPaths = []string{"stdout", "stderr"}
	}
	logger, err := conf.Build(zap.AddStacktrace(zap.DPanicLevel), zap.AddCallerSkip(2))
	if err != nil {
		panic(status.Error(codes.FailedPrecondition, err.Error()))
	}
	defer logger.Sync()
	return &Logger{logger: logger}
}

// Write interface for io.Writer
func (l *Logger) Write(b []byte) (n int, err error) {
	n = len(b)
	if n > 0 && b[n-1] == '\n' {
		b = b[:n-1]
	}
	if l.errorLogger {
		l.WithField("writer", "goxlogger").Error(string(b))
	} else {
		l.WithField("writer", "goxlogger").Info(string(b))
	}
	return n, nil
}

// WriteString interface for io.Writer
func (l *Logger) WriteString(s string) (n int, err error) {
	if l.errorLogger {
		l.WithField("writer", "goxlogger").Error(s)
	} else {
		l.WithField("writer", "goxlogger").Info(s)
	}
	return len(s), nil
}

// ErrorLogger to return a Logger with io.Writer interface for error logs
func (l *Logger) ErrorLogger() *Logger {
	return &Logger{logger: l.Z(), errorLogger: true}
}

// Z return underyling zap logger, this could be removed in coming versions
func (l *Logger) Z() *zap.Logger {
	return l.logger
}

// Namespace will add new path segment joined by dot
func (l *Logger) Namespace(ns string) *Logger {
	return &Logger{
		logger: l.logger.Named(ns).With(zap.String("namespace", ns)),
	}
}

// Sync will flush the logs
func (l *Logger) Sync() error {
	return l.logger.Sync()
}

// WithField return a child Logger with key value preset
func (l *Logger) WithField(key string, value interface{}) *Logger {
	return l.WithFields(Fields{key: value})
}

// WithFields return a child Logger with given key value preset from Fields
func (l *Logger) WithFields(fields Fields) *Logger {
	logger := l.logger
	for k, v := range fields {
		switch t := v.(type) {
		case string:
			logger = logger.With(zap.String(k, t))
		case *string:
			logger = logger.With(zap.String(k, *t))
		case bool:
			logger = logger.With(zap.Bool(k, t))
		case *bool:
			logger = logger.With(zap.Bool(k, *t))
		case uint8:
			logger = logger.With(zap.Uint8(k, t))
		case *uint8:
			logger = logger.With(zap.Uint8(k, *t))
		case uint16:
			logger = logger.With(zap.Uint16(k, t))
		case *uint16:
			logger = logger.With(zap.Uint16(k, *t))
		case uint32:
			logger = logger.With(zap.Uint32(k, t))
		case *uint32:
			logger = logger.With(zap.Uint32(k, *t))
		case uint64:
			logger = logger.With(zap.Uint64(k, t))
		case *uint64:
			logger = logger.With(zap.Uint64(k, *t))
		case int8:
			logger = logger.With(zap.Int8(k, t))
		case *int8:
			logger = logger.With(zap.Int8(k, *t))
		case int16:
			logger = logger.With(zap.Int16(k, t))
		case *int16:
			logger = logger.With(zap.Int16(k, *t))
		case int32:
			logger = logger.With(zap.Int32(k, t))
		case *int32:
			logger = logger.With(zap.Int32(k, *t))
		case int64:
			logger = logger.With(zap.Int64(k, t))
		case *int64:
			logger = logger.With(zap.Int64(k, *t))
		case float32:
			logger = logger.With(zap.Float32(k, t))
		case *float32:
			logger = logger.With(zap.Float32(k, *t))
		case float64:
			logger = logger.With(zap.Float64(k, t))
		case *float64:
			logger = logger.With(zap.Float64(k, *t))
		default:
			logger = logger.With(zap.Any(k, v))
		}
	}
	return &Logger{logger: logger}
}

// Info proxy to underlying Info method call
func (l *Logger) Info(args ...interface{}) {
	l.logger.Sugar().Info(args)
}

// Infof proxy to underlying Infof method call
func (l *Logger) Infof(tmpl string, args ...interface{}) {
	l.logger.Sugar().Infof(tmpl, args)
}

// Debug proxy to underlying Debug method call
func (l *Logger) Debug(args ...interface{}) {
	l.logger.Sugar().Debug(args)
}

// Debugf proxy to underlying Debugf method call
func (l *Logger) Debugf(tmpl string, args ...interface{}) {
	l.logger.Sugar().Debugf(tmpl, args)
}

// Error proxy to underlying Error method call
func (l *Logger) Error(args ...interface{}) {
	l.logger.Sugar().Error(args)
}

// Errorf proxy to underlying Errorf method call
func (l *Logger) Errorf(tmpl string, args ...interface{}) {
	l.logger.Sugar().Errorf(tmpl, args)
}

// Warn proxy to underlying Warn method call
func (l *Logger) Warn(args ...interface{}) {
	l.logger.Sugar().Warn(args)
}

// Warnf proxy to underlying Warnf method call
func (l *Logger) Warnf(tmpl string, args ...interface{}) {
	l.logger.Sugar().Warnf(tmpl, args)
}

// Fatal proxy to underlying Fatal method call
// will exit after call
func (l *Logger) Fatal(args ...interface{}) {
	l.logger.Sugar().Fatal(args)
}

// Fatalf proxy to underlying Fatalf method call
// will exit after call
func (l *Logger) Fatalf(tmpl string, args ...interface{}) {
	l.logger.Sugar().Fatalf(tmpl, args)
}

// Panic proxy to underlying Panic method call
// will exit after call
func (l *Logger) Panic(args ...interface{}) {
	l.logger.Sugar().Panic(args)
}

// Panicf proxy to underlying Panicf method call
// will exit after call
func (l *Logger) Panicf(tmpl string, args ...interface{}) {
	l.logger.Sugar().Panicf(tmpl, args)
}
