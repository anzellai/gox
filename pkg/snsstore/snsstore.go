package snsstore

import (
	"context"
	"encoding/json"
	"os"

	"bitbucket.org/anzellai/gox/pkg/getset"
	"bitbucket.org/anzellai/gox/pkg/goxstore"
	"bitbucket.org/anzellai/gox/pkg/pbsession"
	"bitbucket.org/anzellai/gox/pkg/protomsg"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sns"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var (
	opt = protomsg.DefaultOption
)

// SNSStorer is an implementation of storage engine that does nothing
type SNSStorer struct {
	sess      *session.Session
	snsClient *sns.SNS
}

// New return SNSStorer pointer
func New() *SNSStorer {
	return &SNSStorer{}
}

// SetSession override default AWS Session
func (s *SNSStorer) SetSession(sess *session.Session) *SNSStorer {
	s.sess = sess
	return s
}

// SetClient override default SNS Client
func (s *SNSStorer) SetClient(client *sns.SNS) *SNSStorer {
	s.snsClient = client
	return s
}

// initSession to ensure SNSStorer has AWS session initialised
func (s *SNSStorer) initSession() error {
	if s.sess == nil {
		sess, err := pbsession.New()
		if err != nil {
			return status.Error(codes.FailedPrecondition, err.Error())
		}
		s.sess = sess
	}
	if s.snsClient == nil {
		if os.Getenv("USE_LOCALSTACK") == "1" {
			s.snsClient = sns.New(s.sess, &aws.Config{
				Endpoint: aws.String(os.Getenv("LOCALSTACK_URL")),
			})
		} else {
			s.snsClient = sns.New(s.sess)
		}
	}
	return nil
}

// Close do nothing
func (s *SNSStorer) Close() error {
	s.snsClient = nil
	s.sess = nil
	return nil
}

// Info as in optional health check info operation
func (s *SNSStorer) Info(ctx context.Context, input *goxstore.Input) (output *goxstore.Output, err error) {
	output = &goxstore.Output{}
	if err = s.initSession(); err != nil {
		return
	}

	if input == nil || input.Index == "" {
		output.Info = []byte(`{"info":"ok"}`)
		return
	}

	// if index is given, ping to describe the stream
	var b []byte
	in := &sns.GetTopicAttributesInput{
		TopicArn: aws.String(input.Index),
	}
	snsOutput, err := s.snsClient.GetTopicAttributes(in)
	if err != nil {
		return output, status.Error(codes.FailedPrecondition, err.Error())
	}

	b, err = json.Marshal(snsOutput)
	if err != nil {
		return output, status.Error(codes.DataLoss, err.Error())
	}

	output.Info = b
	return
}

// Get by uuid from proto message, output with single Record in bytes
func (s *SNSStorer) Get(ctx context.Context, input *goxstore.Input) (output *goxstore.Output, err error) {
	return nil, status.Error(codes.Unimplemented, codes.Unimplemented.String())
}

// Query by uuid from proto message, output with slice of Records in bytes
func (s *SNSStorer) Query(ctx context.Context, input *goxstore.Input) (output *goxstore.Output, err error) {
	return nil, status.Error(codes.Unimplemented, codes.Unimplemented.String())
}

// Put by uuid to upsert given proto message, output with slice of Records in bytes
func (s *SNSStorer) Put(ctx context.Context, input *goxstore.Input) (output *goxstore.Output, err error) {
	output = &goxstore.Output{}
	if err = s.initSession(); err != nil {
		return
	}

	if input == nil {
		return output, status.Error(codes.InvalidArgument, codes.InvalidArgument.String())
	}
	b, err := opt.Marshal(input.Record)
	if err != nil {
		return
	}

	in := &sns.PublishInput{}
	if err = opt.Unmarshal(b, in); err != nil {
		return
	}

	in.SetTopicArn(input.Index)
	uid, _ := input.CheckUUID()
	if uid != "" {
		in.SetSubject(uid)
	}

	out, err := s.snsClient.Publish(in)
	if err != nil {
		return output, status.Error(codes.Unavailable, err.Error())
	}
	info, err := opt.Marshal(out)
	if err != nil {
		return output, status.Error(codes.DataLoss, err.Error())
	}

	output = &goxstore.Output{
		Info:   info,
		Count:  1,
		Record: b,
	}

	return
}

// PutBatch by slice of given proto messages, when batch is processed, output only shows count
func (s *SNSStorer) PutBatch(ctx context.Context, input *goxstore.Input) (output *goxstore.Output, err error) {
	output = &goxstore.Output{}
	if err = s.initSession(); err != nil {
		return
	}

	for _, record := range input.Records {
		uid, err := getset.GetAsString(record, "uuid")
		if err != nil {
			return output, status.Errorf(codes.InvalidArgument, err.Error())
		}
		if uid == "" {
			return output, status.Errorf(codes.InvalidArgument, codes.InvalidArgument.String())
		}
		b, err := opt.Marshal(record)
		if err != nil {
			return output, err
		}

		in := &sns.PublishInput{}
		if err = opt.Unmarshal(b, in); err != nil {
			return output, err
		}

		in.SetTopicArn(input.Index)
		in.SetSubject(uid)

		_, err = s.snsClient.Publish(in)
		if err != nil {
			return output, status.Error(codes.Unavailable, err.Error())
		}
	}

	output.Count = len(input.Records)

	return
}

// Delete by uuid from proto message, output with single Record in bytes
func (s *SNSStorer) Delete(ctx context.Context, input *goxstore.Input) (output *goxstore.Output, err error) {
	return nil, status.Error(codes.Unimplemented, codes.Unimplemented.String())
}

// DeleteBatch by slice of given proto messages, when batch is processed, output only shows count
func (s *SNSStorer) DeleteBatch(ctx context.Context, input *goxstore.Input) (output *goxstore.Output, err error) {
	return nil, status.Error(codes.Unimplemented, codes.Unimplemented.String())
}
