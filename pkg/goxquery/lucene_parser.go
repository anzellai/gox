package goxquery

import (
	"encoding/json"
	"fmt"
	"strings"
)

// LuceneParser type definition
type LuceneParser struct{}

// MustParse parse the term input or panic
func (parser LuceneParser) MustParse(terms []Term, after Term, limit int, sortings []Sort) string {
	label, err := parser.Parse(terms, after, limit, sortings)
	if err != nil {
		panic(err)
	}
	return label
}

// Parse will parse the term with extra info required
func (parser LuceneParser) Parse(terms []Term, after Term, limit int, sortings []Sort) (string, error) {
	labels := make([]string, 0)

	for _, term := range terms {
		termLabel, err := parser.ParseTerm(term)
		if err != nil {
			return "", err
		}
		labels = append(labels, termLabel)
	}

	label := strings.Join(labels, " "+string(AND)+" ")
	label = strings.Replace(label, "\"", "'", -1)

	if label == "" {
		label = "*"
	}

	return label, nil

}

// ParseTerm implement QueryParser
func (parser LuceneParser) ParseTerm(term Term) (string, error) {
	var label string

	_operator := func(op Op) string {
		switch op {
		case EQ, CONTAIN:
			return ":"
		case NE:
			return ":"
		}
		return string(op)
	}

	_parseValue := func(value interface{}) (string, error) {
		b, err := json.Marshal(value)
		if err != nil {
			return "", err
		}
		return string(b), nil
	}

	_parseUnescapedValue := func(value interface{}) (string, error) {
		var i interface{}
		val, err := _parseValue(value)
		if err != nil {
			return "", err
		}
		if err := json.Unmarshal([]byte(val), &i); err != nil {
			return "", err
		}
		return fmt.Sprintf("%v", i), nil
	}

	_parseField := func(field string) string {
		// return strings.Replace(field, ".", "\\.", -1)
		return field
	}

	switch v := term.Value.(type) {
	case []Term:
		labels := make([]string, 0)
		rangeTerms := make(map[string][]string)

		for _, t := range v {

			switch t.Op {
			case GT, GTE:
				rangeLabel, err := _parseUnescapedValue(t.Value)
				if err != nil {
					return "", err
				}

				rangeTerm, ok := rangeTerms[t.Field]
				if ok {
					rangeTerm[0] = rangeLabel
				} else {
					rangeTerms[t.Field] = []string{rangeLabel, "*"}
				}
				continue

			case LT, LTE:
				rangeLabel, err := _parseUnescapedValue(t.Value)
				if err != nil {
					return "", err
				}

				rangeTerm, ok := rangeTerms[t.Field]
				if ok {
					rangeTerm[1] = rangeLabel
				} else {
					rangeTerms[t.Field] = []string{"*", rangeLabel}
				}
				continue

			default:
				termLabel, err := parser.ParseTerm(t)
				if err != nil {
					return "", err
				}
				labels = append(labels, termLabel)
			}
		}

		if len(rangeTerms) > 0 {
			for field, rangeTerm := range rangeTerms {
				labels = append(labels, fmt.Sprintf("%s:[%s TO %s]", _parseField(field), rangeTerm[0], rangeTerm[1]))
			}
		}

		label = strings.Join(labels, " "+_operator(term.Op)+" ")
		return fmt.Sprintf("(%s)", label), nil

	default:

		switch term.Op {
		case GT, GTE:
			termLabel, err := _parseUnescapedValue(term.Value)
			if err != nil {
				return "", err
			}
			label = fmt.Sprintf("[%v TO *]", termLabel)

		case LT, LTE:
			termLabel, err := _parseUnescapedValue(term.Value)
			if err != nil {
				return "", err
			}
			label = fmt.Sprintf("[* TO %v]", termLabel)

		default:
			termLabel, err := _parseValue(v)
			if err != nil {
				return "", err
			}
			label = termLabel
		}
	}

	label = strings.Replace(label, "\"", "'", -1)
	if label == "" {
		label = "*"
	}
	// FIXME: for keyword term label, all quotes are removed as it must match exactly
	// if needed to match term with quotes, use CONTAIN for now
	if term.Op == EQ {
		label = strings.Replace(label, "'", "", -1)
		return fmt.Sprintf("(%s.keyword%s%s)", _parseField(term.Field), _operator(term.Op), label), nil
	}
	if term.Op == NE {
		label = strings.Replace(label, "'", "", -1)
		return fmt.Sprintf("!(%s.keyword%s%s)", _parseField(term.Field), _operator(term.Op), label), nil
	}
	return fmt.Sprintf("(%s%s%s)", _parseField(term.Field), _operator(term.Op), label), nil
}

// ParseSorts implement QueryParser
func (parser LuceneParser) ParseSorts(sortings ...Sort) []string {
	labels := make([]string, 0)
	for _, sorting := range sortings {
		labels = append(labels, fmt.Sprintf(`{"%s":{"order":"%s"}}`, sorting.Field, sorting.Direction))
	}
	labels = append(labels, `{"uuid.keyword":{"order":"DESC"}}`)
	return labels
}
