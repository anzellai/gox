package goxquery

import (
	"encoding/json"
	"fmt"
	"sort"
	"strings"

	"bitbucket.org/anzellai/gox/pkg/getset"
)

// QueryParser as in DSL Parser, default to Gox
type QueryParser interface {
	Parse(terms []Term, after Term, limit int, sortings []Sort) (string, error)
	MustParse(terms []Term, after Term, limit int, sortings []Sort) string
	ParseTerm(Term) (string, error)
	ParseSorts(sortings ...Sort) []string
}

// Op as in operator grammar
type Op string

const (
	AND     Op = "AND"
	OR      Op = "OR"
	CONTAIN Op = "CONTAIN"
	EQ      Op = "="
	NE      Op = "!="
	GT      Op = ">"
	GTE     Op = ">="
	LT      Op = "<"
	LTE     Op = "<="
)

// Direction as in sorting direction
type Direction string

const (
	ASC  Direction = "ASC"
	DESC Direction = "DESC"
)

// Sort as in sorting grammar
type Sort struct {
	Field     string
	Direction Direction
}

// Term as in querying term
type Term struct {
	Field string
	Op    Op
	Value interface{}
}

// NewTerm as in Term constructor
func NewTerm(field string, op Op, value interface{}) Term {
	return Term{Field: field, Op: op, Value: value}
}

// NewSubTerm as in Terms wrapped in an Op for NestedBy query
func NewSubTerm(op Op, terms ...Term) Term {
	return NewTerm("", op, terms)
}

// Query as in root query builder, containing all querying information
type Query struct {
	Terms  []Term
	After  Term
	Limit  int
	Sorts  []Sort
	Parser QueryParser
}

// NewQuery return initialised Query instance with sane defaults
func NewQuery() *Query {
	return &Query{
		Terms:  make([]Term, 0),
		Limit:  10000,
		Sorts:  make([]Sort, 0),
		Parser: new(GoxParser),
	}
}

// SetParser set custom parser on Query engine
func (q *Query) SetParser(parser QueryParser) *Query {
	q.Parser = parser
	return q
}

// SetLimit set limit on Query
func (q *Query) SetLimit(limit int) *Query {
	q.Limit = limit
	return q
}

// SetAfter set after on Query for scanning after or scrolling id
func (q *Query) SetAfter(field string, value interface{}) *Query {
	q.After = Term{Field: field, Op: EQ, Value: value}
	return q
}

// SortBy add sorting to Query
func (q *Query) SortBy(field string, direction Direction) *Query {
	q.Sorts = append(q.Sorts, Sort{Field: field, Direction: direction})
	return q
}

// FilterBy as in short hand to add individual Term into root Query
func (q *Query) FilterBy(field string, op Op, value interface{}) *Query {
	q.Terms = append(q.Terms, Term{Field: field, Op: op, Value: value})
	return q
}

// NestedBy as in short hand to add a series of nested child Terms into root Query
func (q *Query) NestedBy(op Op, children ...Term) *Query {
	q.Terms = append(q.Terms, Term{Field: "", Op: op, Value: children})
	return q
}

// Parse to transform root Query into string label
func (q *Query) Parse() (string, error) {
	parsed, err := q.Parser.Parse(q.Terms, q.After, q.Limit, q.Sorts)
	return parsed, err
}

// MustParse to Parse or panic
func (q *Query) MustParse() string {
	return q.Parser.MustParse(q.Terms, q.After, q.Limit, q.Sorts)
}

// ParseSorts implement QueryParser
func (q *Query) ParseSorts() []string {
	return q.Parser.ParseSorts()
}

// Eval to evaluate root Query, combining logics from Query's Terms + After + Limit + Sorts
func (q *Query) Eval(items []Item) []Item {
	if len(q.Sorts) > 0 {
		sortedItems := sorted{
			items: items,
			sorts: q.Sorts,
		}
		sort.Sort(sortedItems)
		items = sortedItems.items
	}

	matched := make([]Item, 0)

	for _, item := range items {
		ok := true
		for _, term := range q.Terms {
			ok = q.eval(term, item)
			if !ok {
				break
			}
		}

		if ok {
			if q.After.Field != "" && q.After.Value != nil {
				after := getBy(item, q.After.Field)
				if parseUnescapedValue(after) > parseUnescapedValue(q.After.Value) {
					continue
				}
			}
			matched = append(matched, item)
			if len(matched) >= q.Limit {
				return matched
			}
		}
	}

	return matched
}

// eval to evaluate individual Term and its logic against provided Item
func (q *Query) eval(term Term, item Item) bool {
	ok := true

	if term.Op == AND {

		nodes := term.Value.([]Term)
		for _, node := range nodes {
			if !q.eval(node, item) {
				ok = false
				break
			}
		}

	} else if term.Op == OR {

		nodes := term.Value.([]Term)
		for _, node := range nodes {
			if q.eval(node, item) {
				ok = true
				break
			} else {
				ok = false
			}
		}

	} else {

		compareValue := getBy(item, term.Field)
		compareLabel := parseUnescapedValue(compareValue)
		label := parseUnescapedValue(term.Value)

		switch term.Op {
		case EQ:
			ok = compareLabel == label
		case NE:
			ok = compareLabel != label
		case GT:
			ok = compareLabel > label
		case GTE:
			ok = compareLabel >= label
		case LT:
			ok = compareLabel < label
		case LTE:
			ok = compareLabel <= label
		case CONTAIN:
			ok = strings.Contains(compareLabel, label)
		}

	}

	return ok
}

/*
	Type aliases and custom sorting implementation
*/

// Item as a short hand of map[string]interface{} type
type Item = map[string]interface{}

// sorted as in comparable type for slice of Items
type sorted struct {
	items []Item
	sorts []Sort
}

// Len implements sort.Sort interface
func (s sorted) Len() int {
	return len(s.items)
}

// Swap implements sort.Sort interface
func (s sorted) Swap(i, j int) {
	s.items[i], s.items[j] = s.items[j], s.items[i]
}

// Less implements sort.Sort interface
func (s sorted) Less(i, j int) bool {
	for _, sorting := range s.sorts {
		iValue := getBy(s.items[i], sorting.Field)
		jValue := getBy(s.items[j], sorting.Field)

		if parseUnescapedValue(iValue) == parseUnescapedValue(jValue) {
			continue
		}

		if sorting.Direction == DESC {
			if iValue == nil && jValue != nil {
				return false
			} else if iValue != nil && jValue == nil {
				return true
			}
			return parseUnescapedValue(iValue) > parseUnescapedValue(jValue)
		}

		if iValue == nil && jValue != nil {
			return true
		} else if iValue != nil && jValue == nil {
			return false
		}
		return parseUnescapedValue(iValue) < parseUnescapedValue(jValue)
	}
	return true
}

/*
	Helper functions
*/

// passOrFail turn boolean to string label
func passOrFail(ok bool) string {
	if ok {
		return "PASS"
	}
	return "FAIL"
}

// parseValue turn value interface into string label
func parseValue(value interface{}) string {
	b, err := json.Marshal(value)
	if err != nil {
		panic(err)
	}
	return string(b)
}

// parseUnescapedValue turn value interface into string literal without quotes
func parseUnescapedValue(value interface{}) string {
	if value == "" {
		return ""
	}
	var i interface{}
	val := parseValue(value)
	if err := json.Unmarshal([]byte(val), &i); err != nil {
		panic(err)
	}
	return fmt.Sprintf("%v", i)
}

// getBy as in short hand to get JSON's key path access as interface value
func getBy(item Item, key string) interface{} {
	value, err := getset.Get(item, key)
	if err != nil {
		panic(err)
	}
	return value
}
