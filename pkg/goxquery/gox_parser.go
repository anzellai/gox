package goxquery

import (
	"encoding/json"
	"fmt"
	"strings"
)

// GoxParser type definition
type GoxParser struct{}

// MustParse parse the term input or panic
func (parser GoxParser) MustParse(terms []Term, after Term, limit int, sortings []Sort) string {
	label, err := parser.Parse(terms, after, limit, sortings)
	if err != nil {
		panic(err)
	}
	return label
}

// Parse will parse the term with extra info required
func (parser GoxParser) Parse(terms []Term, after Term, limit int, sortings []Sort) (string, error) {
	labels := make([]string, 0)

	for _, term := range terms {
		termLabel, err := parser.ParseTerm(term)
		if err != nil {
			return "", err
		}
		labels = append(labels, termLabel)
	}
	label := strings.Join(labels, " "+string(AND)+" ")

	if after.Field != "" {
		afterLabel, err := parser.ParseTerm(after)
		if err != nil {
			return "", err
		}
		label = label + " AFTER " + afterLabel
	}

	if limit != 0 {
		label = fmt.Sprintf("%s LIMIT %d", label, limit)
	}

	if len(sortings) > 0 {
		sortLabels := parser.ParseSorts(sortings...)
		label = fmt.Sprintf("%s BY %s", label, strings.Join(sortLabels, ","))
	}

	return label, nil

}

// ParseTerm implement QueryParser
func (parser GoxParser) ParseTerm(term Term) (string, error) {
	var label string
	_parseValue := func(value interface{}) (string, error) {
		b, err := json.Marshal(value)
		if err != nil {
			return "", err
		}
		return string(b), nil
	}
	switch v := term.Value.(type) {
	case []Term:
		labels := make([]string, 0)
		for _, t := range v {
			each, err := parser.ParseTerm(t)
			if err != nil {
				return "", err
			}
			labels = append(labels, each)
		}
		label = strings.Join(labels, " "+string(term.Op)+" ")
		return fmt.Sprintf("(%s)", label), nil

	default:
		valueLabel, err := _parseValue(v)
		if err != nil {
			return "", err
		}
		label = valueLabel
	}

	return fmt.Sprintf("(%s %s %s)", term.Field, string(term.Op), label), nil
}

// ParseSorts implement QueryParser
func (parser GoxParser) ParseSorts(sortings ...Sort) []string {
	labels := make([]string, 0)
	for _, sorting := range sortings {
		labels = append(labels, fmt.Sprintf("%s:%s", sorting.Field, sorting.Direction))
	}
	return labels
}
