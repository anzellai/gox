package mapstore

import (
	"context"
	"fmt"
	"strings"
	"sync"
	"time"

	"bitbucket.org/anzellai/gox/pkg/getset"
	"bitbucket.org/anzellai/gox/pkg/goxstore"
	"bitbucket.org/anzellai/gox/pkg/protomsg"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var (
	opt = protomsg.DefaultOption
)

// MAPStorer is an implementation of in-memory map as a storage
// this would be useful for local development
type MAPStorer struct {
	m map[string]interface{}
	s sync.Mutex
}

// New return initialised MAPStorer pointer
func New() *MAPStorer {
	ms := &MAPStorer{
		m: make(map[string]interface{}),
	}
	go func() {
		for now := range time.Tick(time.Second) {
			ms.s.Lock()
			defer ms.s.Unlock()
			for key, val := range ms.m {
				ttl, err := getset.GetAsInt64(val, "ttl")
				if err != nil {
					continue
				}
				if ttl > 0 && ttl < now.UTC().Unix() {
					delete(ms.m, key)
				}
			}
		}
	}()
	return ms
}

// Close clean up map
func (s *MAPStorer) Close() error {
	s.m = nil
	return nil
}

func buildKey(input *goxstore.Input) (key string, err error) {
	uid, err := input.CheckUUID()
	if err != nil {
		return "", err
	}
	key = fmt.Sprintf("%s__%s", input.Index, uid)
	return
}

// Info as in optional health check info operation
func (s *MAPStorer) Info(ctx context.Context, _ *goxstore.Input) (output *goxstore.Output, err error) {
	return &goxstore.Output{Info: []byte(`{"info":"ok"}`)}, nil
}

// Get by uuid from proto message, output with single Record in bytes
func (s *MAPStorer) Get(ctx context.Context, input *goxstore.Input) (output *goxstore.Output, err error) {
	output = &goxstore.Output{}
	key, err := buildKey(input)
	if err != nil {
		return
	}

	record, ok := s.m[key]
	if !ok {
		return output, status.Error(codes.NotFound, codes.NotFound.String())
	}
	b, err := opt.Marshal(record)
	if err != nil {
		return
	}
	if input.TTL {
		now := time.Now().UTC().Format(time.RFC3339)
		ttl, err := getset.GetAsString(record, "ttl")
		if err != nil {
			return output, status.Error(codes.DataLoss, err.Error())
		}
		if ttl != "" && ttl < now {
			return output, status.Error(codes.NotFound, codes.NotFound.String())
		}
	}

	output.Record = b
	output.Count = 1

	return
}

// Query by uuid from proto message, output with slice of Records in bytes
func (s *MAPStorer) Query(ctx context.Context, input *goxstore.Input) (output *goxstore.Output, err error) {
	output = &goxstore.Output{}
	ms := make([]map[string]interface{}, 0)
	for key, value := range s.m {
		if strings.HasPrefix(key, input.Index+"__") {
			if input.TTL {
				now := time.Now().UTC().Format(time.RFC3339)
				ttl, err := getset.GetAsString(value, "ttl")
				if err != nil {
					return output, status.Error(codes.DataLoss, err.Error())
				}
				if ttl != "" && ttl < now {
					continue
				}
			}
			b, err := opt.Marshal(value)
			if err != nil {
				return output, status.Error(codes.DataLoss, err.Error())
			}
			m := make(map[string]interface{})
			if err = opt.Unmarshal(b, &m); err != nil {
				return output, status.Error(codes.DataLoss, err.Error())
			}
			ms = append(ms, m)
		}
	}

	ms = input.Query.Eval(ms)

	b, err := opt.Marshal(ms)
	if err != nil {
		return
	}

	output.Records = b
	output.Count = len(ms)

	return
}

// Put by uuid to upsert given proto message, output with slice of Records in bytes
func (s *MAPStorer) Put(ctx context.Context, input *goxstore.Input) (output *goxstore.Output, err error) {
	output = &goxstore.Output{}
	key, err := buildKey(input)
	if err != nil {
		return
	}
	b, err := opt.Marshal(input.Record)
	if err != nil {
		return
	}

	m := make(map[string]interface{})
	err = opt.Unmarshal(b, &m)
	if err != nil {
		return
	}

	s.m[key] = m

	output.Count = 1
	output.Record = b

	return
}

// PutBatch by slice of given proto messages, when batch is processed, output only shows count
func (s *MAPStorer) PutBatch(ctx context.Context, input *goxstore.Input) (output *goxstore.Output, err error) {
	output = &goxstore.Output{}
	maps := make([]map[string]interface{}, 0)

	for _, record := range input.Records {
		in := &goxstore.Input{Index: input.Index, Record: record, TTL: input.TTL}
		key, err := buildKey(in)
		if err != nil {
			return output, err
		}
		b, err := opt.Marshal(input.Record)
		if err != nil {
			return output, err
		}
		m := make(map[string]interface{})
		if err = opt.Unmarshal(b, &m); err != nil {
			return output, err
		}
		s.m[key] = m
		maps = append(maps, m)
		output.Count += 1
	}

	b, err := opt.Marshal(maps)
	if err != nil {
		return
	}
	output.Record = b

	return
}

// Delete by uuid from proto message, output with single Record in bytes
func (s *MAPStorer) Delete(ctx context.Context, input *goxstore.Input) (output *goxstore.Output, err error) {
	output = &goxstore.Output{}
	key, err := buildKey(input)
	if err != nil {
		return
	}

	output, err = s.Get(ctx, input)
	if err != nil {
		return
	}
	delete(s.m, key)

	return
}

// DeleteBatch by slice of given proto messages, when batch is processed, output only shows count
func (s *MAPStorer) DeleteBatch(ctx context.Context, input *goxstore.Input) (output *goxstore.Output, err error) {
	output = &goxstore.Output{}
	maps := make([]map[string]interface{}, 0)

	for _, record := range input.Records {
		in := &goxstore.Input{Index: input.Index, Record: record}
		key, err := buildKey(in)
		if err != nil {
			return output, err
		}
		existed, ok := s.m[key]
		if !ok {
			return output, status.Error(codes.NotFound, codes.NotFound.String())
		}
		b, err := opt.Marshal(existed)
		if err != nil {
			return output, err
		}
		m := make(map[string]interface{})
		if err = opt.Unmarshal(b, &m); err != nil {
			return output, err
		}
		maps = append(maps, m)
		output.Count += 1
		s.m[key] = nil
	}

	b, err := opt.Marshal(maps)
	if err != nil {
		return
	}
	output.Record = b

	return
}
