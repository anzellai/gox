package bdbstore

import (
	"context"
	"os"
	"path/filepath"
	"time"

	"bitbucket.org/anzellai/gox/pkg/getset"
	"bitbucket.org/anzellai/gox/pkg/goxstore"
	"bitbucket.org/anzellai/gox/pkg/protomsg"
	bolt "go.etcd.io/bbolt"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var (
	opt = protomsg.DefaultOption
)

// BDBStorer is an implementation of storage engine that does nothing
type BDBStorer struct {
	db *bolt.DB
}

// New return BDBStorer pointer
func New() *BDBStorer {
	return &BDBStorer{}
}

func (s *BDBStorer) initSession() error {
	if s.db == nil {
		db, err := bolt.Open(filepath.Join(os.TempDir(), "gox_bdbstorer.db"), 0600, nil)
		if err != nil {
			return status.Error(codes.FailedPrecondition, err.Error())
		}
		s.db = db
	}
	return nil
}

// Close clean up unused session
func (s *BDBStorer) Close() error {
	if s.db == nil {
		return nil
	}
	return s.db.Close()
}

// Info as in optional health check info operation
func (s *BDBStorer) Info(ctx context.Context, _ *goxstore.Input) (output *goxstore.Output, err error) {
	output = &goxstore.Output{}
	if err = s.initSession(); err != nil {
		return
	}
	info := s.db.Info()
	b, err := opt.Marshal(info)
	if err != nil {
		return
	}
	output.Info = b

	return
}

// Get by uuid from proto message, output with single Record in bytes
func (s *BDBStorer) Get(ctx context.Context, input *goxstore.Input) (output *goxstore.Output, err error) {
	output = &goxstore.Output{}
	if err = s.initSession(); err != nil {
		return
	}
	err = s.db.View(func(tx *bolt.Tx) error {
		uid, err := input.CheckUUID()
		if err != nil {
			return err
		}
		b := tx.Bucket([]byte(input.Index))
		if b == nil {
			return status.Error(codes.NotFound, codes.NotFound.String())
		}
		v := b.Get([]byte(uid))
		if v == nil {
			return status.Error(codes.NotFound, codes.NotFound.String())
		}
		if input.TTL {
			m := make(map[string]interface{})
			if err = opt.Unmarshal(v, &m); err != nil {
				return status.Error(codes.DataLoss, err.Error())
			}
			now := time.Now().UTC().Format(time.RFC3339)
			ttl, err := getset.GetAsString(m, "ttl")
			if err != nil {
				return status.Error(codes.DataLoss, err.Error())
			}
			if ttl != "" && ttl < now {
				return status.Error(codes.NotFound, codes.NotFound.String())
			}
		}
		output.Count = 1
		output.Record = v
		return nil
	})

	return
}

// Query by uuid from proto message, output with slice of Records in bytes
func (s *BDBStorer) Query(ctx context.Context, input *goxstore.Input) (output *goxstore.Output, err error) {
	output = &goxstore.Output{}
	if err = s.initSession(); err != nil {
		return
	}
	if input.Query == nil {
		return output, status.Error(codes.InvalidArgument, codes.Internal.String())
	}
	batch := make([]map[string]interface{}, 0)
	err = s.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(input.Index))
		if b == nil {
			return status.Error(codes.NotFound, codes.NotFound.String())
		}

		return b.ForEach(func(k, v []byte) error {
			i := make(map[string]interface{})
			if err := opt.Unmarshal(v, &i); err != nil {
				return err
			}

			if input.TTL {
				now := time.Now().UTC().Format(time.RFC3339)
				ttl, err := getset.GetAsString(i, "ttl")
				if err != nil {
					return status.Error(codes.DataLoss, err.Error())
				}
				if ttl != "" && ttl < now {
					return status.Error(codes.NotFound, codes.NotFound.String())
				}
			} else {
				batch = append(batch, i)
			}

			return nil
		})
	})

	batch = input.Query.Eval(batch)

	if err != nil {
		s := status.Convert(err)
		err = status.Error(codes.FailedPrecondition, s.Err().Error())
		return
	}
	records, err := opt.Marshal(batch)
	if err != nil {
		return
	}
	output.Count = len(batch)
	output.Records = records
	return
}

// Put by uuid to upsert given proto message, output with slice of Records in bytes
func (s *BDBStorer) Put(ctx context.Context, input *goxstore.Input) (output *goxstore.Output, err error) {
	output = &goxstore.Output{}
	if err = s.initSession(); err != nil {
		return
	}

	err = s.db.Update(func(tx *bolt.Tx) error {
		uid, err := input.CheckUUID()
		if err != nil {
			return err
		}
		record, err := opt.Marshal(input.Record)
		if err != nil {
			return err
		}
		b, err := tx.CreateBucketIfNotExists([]byte(input.Index))
		if err != nil {
			return status.Error(codes.FailedPrecondition, err.Error())
		}
		if err := b.Put([]byte(uid), record); err != nil {
			return status.Error(codes.FailedPrecondition, err.Error())
		}
		output.Record = record
		output.Count = 1
		return nil
	})

	return
}

// PutBatch by slice of given proto messages, when batch is processed, output only shows count
func (s *BDBStorer) PutBatch(ctx context.Context, input *goxstore.Input) (output *goxstore.Output, err error) {
	output = &goxstore.Output{}
	if err = s.initSession(); err != nil {
		return
	}
	records := make([]map[string]interface{}, 0)

	err = s.db.Batch(func(tx *bolt.Tx) error {
		b, err := tx.CreateBucketIfNotExists([]byte(input.Index))
		if err != nil {
			return status.Error(codes.FailedPrecondition, err.Error())
		}
		for _, record := range input.Records {
			recordInput := &goxstore.Input{Index: input.Index, Record: record, TTL: input.TTL}
			uid, err := recordInput.CheckUUID()
			if err != nil {
				return err
			}
			r, err := opt.Marshal(record)
			if err != nil {
				return err
			}
			if err = b.Put([]byte(uid), r); err != nil {
				return status.Error(codes.FailedPrecondition, err.Error())
			}
			m := make(map[string]interface{})
			if err = opt.Unmarshal(r, &m); err != nil {
				return err
			}
			records = append(records, m)
			output.Count++
		}
		return nil
	})
	if err != nil {
		return
	}
	b, err := opt.Marshal(records)
	if err != nil {
		return
	}
	output.Records = b

	return
}

// Delete by uuid from proto message, output with single Record in bytes
func (s *BDBStorer) Delete(ctx context.Context, input *goxstore.Input) (output *goxstore.Output, err error) {
	output = &goxstore.Output{}
	if err = s.initSession(); err != nil {
		return
	}

	err = s.db.Update(func(tx *bolt.Tx) error {
		uid, err := input.CheckUUID()
		if err != nil {
			return err
		}
		b := tx.Bucket([]byte(input.Index))
		if b == nil {
			return status.Error(codes.NotFound, codes.NotFound.String())
		}
		v := b.Get([]byte(uid))
		if v == nil {
			return status.Error(codes.NotFound, codes.NotFound.String())
		}
		if err = b.Delete([]byte(uid)); err != nil {
			return status.Error(codes.FailedPrecondition, err.Error())
		}
		output.Record = v
		output.Count = 1
		return nil
	})

	return
}

// DeleteBatch by slice of given proto messages, when batch is processed, output only shows count
func (s *BDBStorer) DeleteBatch(ctx context.Context, input *goxstore.Input) (output *goxstore.Output, err error) {
	output = &goxstore.Output{}
	if err = s.initSession(); err != nil {
		return
	}

	records := make([]map[string]interface{}, 0)

	err = s.db.Batch(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(input.Index))
		if b == nil {
			return status.Error(codes.NotFound, codes.NotFound.String())
		}
		for _, record := range input.Records {
			recordInput := &goxstore.Input{Index: input.Index, Record: record}
			uid, err := recordInput.CheckUUID()
			if err != nil {
				return err
			}
			v := b.Get([]byte(uid))
			if v == nil {
				return status.Error(codes.NotFound, codes.NotFound.String())
			}
			if err = b.Delete([]byte(uid)); err != nil {
				return status.Error(codes.FailedPrecondition, err.Error())
			}
			m := make(map[string]interface{})
			if err = opt.Unmarshal(v, &m); err != nil {
				return err
			}
			records = append(records, m)
			output.Count++
		}
		return nil
	})
	if err != nil {
		return
	}
	b, err := opt.Marshal(records)
	if err != nil {
		return
	}
	output.Records = b

	return
}
