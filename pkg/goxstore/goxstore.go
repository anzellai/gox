package goxstore

import (
	"context"
	"os"

	"bitbucket.org/anzellai/gox/pkg/getset"
	"bitbucket.org/anzellai/gox/pkg/goxquery"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// Input type definition for use in GoxStorer interface
type Input struct {
	Index   string          // Index as in table or index name
	Key     string          // Key as in uuid for its record
	TTL     bool            // TTL as in Time To Live for the record
	Query   *goxquery.Query // Query as in constructed goxquery.Query type
	Record  interface{}     // Item as in single proto message type
	Records []interface{}   // Items as in slice of proto messages type
}

// CheckUUID return its Record uuid field string value or error
func (input *Input) CheckUUID() (uid string, err error) {
	if input.Key != "" {
		uid = input.Key
	} else {
		uid, err = getset.GetAsString(input.Record, "uuid")
		if err != nil {
			return "", status.Error(codes.InvalidArgument, err.Error())
		}
		input.Key = uid
	}
	return
}

// JSON return Go map type referencing non-record value information
// useful for logging and debugging
func (input *Input) JSON() map[string]interface{} {
	uid, _ := input.CheckUUID()
	if input.Key != "" {
		uid = input.Key
	}
	return map[string]interface{}{
		"Index":   input.Index,
		"Key":     uid,
		"Query":   input.Query,
		"Record":  input.Record != nil,
		"Records": len(input.Records),
	}
}

// GetEnv return env variable if key existed or returning the fallback
func GetEnv(key, fallback string) string {
	if val := os.Getenv(key); val != "" {
		return val
	}
	return fallback
}

// Output type definition for use in GoxStorer interface
type Output struct {
	Count   int
	Info    []byte
	Record  []byte
	Records []byte
}

// GoxStorer interface for defining CRUD and Key/Value access for implemented storage engine
type GoxStorer interface {
	// Close as in finaliser
	Close() error
	// Info as in optional health check info operation
	Info(ctx context.Context, input *Input) (output *Output, err error)
	// Get by uuid from proto message, output with single Record in bytes
	Get(ctx context.Context, input *Input) (output *Output, err error)
	// Query by uuid from proto message, output with slice of Records in bytes
	Query(ctx context.Context, input *Input) (output *Output, err error)
	// Put by uuid to upsert given proto message, output with slice of Records in bytes
	Put(ctx context.Context, input *Input) (output *Output, err error)
	// PutBatch by slice of given proto messages, when batch is processed, output only shows count
	PutBatch(ctx context.Context, input *Input) (output *Output, err error)
	// Delete by uuid from proto message, output with single Record in bytes
	Delete(ctx context.Context, input *Input) (output *Output, err error)
	// DeleteBatch by slice of given proto messages, when batch is processed, output only shows count
	DeleteBatch(ctx context.Context, input *Input) (output *Output, err error)
}

// NoopStorer is an implementation of storage engine that does nothing
type NoopStorer struct{}

// New return NoopStorer pointer
func New() *NoopStorer {
	return &NoopStorer{}
}

// Close do nothing
func (s *NoopStorer) Close() error {
	return nil
}

// Info as in optional health check info operation
func (s *NoopStorer) Info(ctx context.Context, input *Input) (output *Output, err error) {
	return &Output{}, nil
}

// Get by uuid from proto message, output with single Record in bytes
func (s *NoopStorer) Get(ctx context.Context, input *Input) (output *Output, err error) {
	return &Output{}, nil
}

// Query by uuid from proto message, output with slice of Records in bytes
func (s *NoopStorer) Query(ctx context.Context, input *Input) (output *Output, err error) {
	return &Output{}, nil
}

// Put by uuid to upsert given proto message, output with slice of Records in bytes
func (s *NoopStorer) Put(ctx context.Context, input *Input) (output *Output, err error) {
	return &Output{}, nil
}

// PutBatch by slice of given proto messages, when batch is processed, output only shows count
func (s *NoopStorer) PutBatch(ctx context.Context, input *Input) (output *Output, err error) {
	return &Output{}, nil
}

// Delete by uuid from proto message, output with single Record in bytes
func (s *NoopStorer) Delete(ctx context.Context, input *Input) (output *Output, err error) {
	return &Output{}, nil
}

// DeleteBatch by slice of given proto messages, when batch is processed, output only shows count
func (s *NoopStorer) DeleteBatch(ctx context.Context, input *Input) (output *Output, err error) {
	return &Output{}, nil
}
