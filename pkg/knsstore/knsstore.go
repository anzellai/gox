package knsstore

import (
	"context"
	"encoding/json"
	"os"
	"strings"

	"bitbucket.org/anzellai/gox/pkg/getset"
	"bitbucket.org/anzellai/gox/pkg/goxstore"
	"bitbucket.org/anzellai/gox/pkg/pbsession"
	"bitbucket.org/anzellai/gox/pkg/protomsg"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/kinesis"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var (
	opt = protomsg.DefaultOption
)

// KNSStorer is an implementation of storage engine that does nothing
type KNSStorer struct {
	sess      *session.Session
	knsClient *kinesis.Kinesis
}

// New return KNSStorer pointer
func New() *KNSStorer {
	return &KNSStorer{}
}

// SetSession override default AWS Session
func (s *KNSStorer) SetSession(sess *session.Session) *KNSStorer {
	s.sess = sess
	return s
}

// SetClient override default KNS Client
func (s *KNSStorer) SetClient(client *kinesis.Kinesis) *KNSStorer {
	s.knsClient = client
	return s
}

// initSession to ensure KNSStorer has AWS session initialised
func (s *KNSStorer) initSession() error {
	if s.sess == nil {
		sess, err := pbsession.New()
		if err != nil {
			return status.Error(codes.FailedPrecondition, err.Error())
		}
		s.sess = sess
	}
	if s.knsClient == nil {
		if os.Getenv("USE_LOCALSTACK") == "1" {
			s.knsClient = kinesis.New(s.sess, &aws.Config{
				Endpoint: aws.String(os.Getenv("LOCALSTACK_URL")),
			})
		} else {
			s.knsClient = kinesis.New(s.sess)
		}
	}
	return nil
}

// Close do nothing
func (s *KNSStorer) Close() error {
	s.knsClient = nil
	s.sess = nil
	return nil
}

// Info as in optional health check info operation
func (s *KNSStorer) Info(ctx context.Context, input *goxstore.Input) (output *goxstore.Output, err error) {
	output = &goxstore.Output{}
	if err = s.initSession(); err != nil {
		return
	}

	if input == nil || input.Index == "" {
		output.Info = []byte(`{"info":"ok"}`)
		return
	}

	// if index is given, ping to describe the stream
	var b []byte
	in := &kinesis.DescribeStreamInput{StreamName: aws.String(input.Index)}
	knsOutput, err := s.knsClient.DescribeStream(in)
	if err != nil {
		return output, status.Error(codes.FailedPrecondition, err.Error())
	}

	b, err = json.Marshal(knsOutput)
	if err != nil {
		return output, status.Error(codes.DataLoss, err.Error())
	}

	output.Info = b
	return
}

// Get by uuid from proto message, output with single Record in bytes
func (s *KNSStorer) Get(ctx context.Context, input *goxstore.Input) (output *goxstore.Output, err error) {
	return nil, status.Error(codes.Unimplemented, codes.Unimplemented.String())
}

// Query by uuid from proto message, output with slice of Records in bytes
func (s *KNSStorer) Query(ctx context.Context, input *goxstore.Input) (output *goxstore.Output, err error) {
	return nil, status.Error(codes.Unimplemented, codes.Unimplemented.String())
}

// Put by uuid to upsert given proto message, output with slice of Records in bytes
func (s *KNSStorer) Put(ctx context.Context, input *goxstore.Input) (output *goxstore.Output, err error) {
	output = &goxstore.Output{}
	if err = s.initSession(); err != nil {
		return
	}

	if input == nil {
		return output, status.Error(codes.InvalidArgument, codes.InvalidArgument.String())
	}
	b, err := opt.Marshal(input.Record)
	if err != nil {
		return
	}

	in := &kinesis.PutRecordInput{
		StreamName: aws.String(input.Index),
		Data:       b,
	}
	uid, _ := input.CheckUUID()
	if uid != "" {
		in.SetPartitionKey(uid)
	}

	out, err := s.knsClient.PutRecord(in)
	if err != nil {
		return output, status.Error(codes.Unavailable, err.Error())
	}
	info, err := opt.Marshal(out)
	if err != nil {
		return output, status.Error(codes.DataLoss, err.Error())
	}

	output = &goxstore.Output{
		Info:   info,
		Count:  1,
		Record: b,
	}

	return
}

// PutBatch by slice of given proto messages, when batch is processed, output only shows count
func (s *KNSStorer) PutBatch(ctx context.Context, input *goxstore.Input) (output *goxstore.Output, err error) {
	output = &goxstore.Output{}
	if err = s.initSession(); err != nil {
		return
	}

	in := &kinesis.PutRecordsInput{
		StreamName: aws.String(input.Index),
	}
	records := make([]*kinesis.PutRecordsRequestEntry, 0)

	for _, record := range input.Records {
		uid, err := getset.GetAsString(record, "uuid")
		if err != nil {
			return output, status.Errorf(codes.InvalidArgument, err.Error())
		}
		if uid == "" {
			return output, status.Errorf(codes.InvalidArgument, codes.InvalidArgument.String())
		}
		b, err := opt.Marshal(record)
		if err != nil {
			return output, err
		}
		records = append(records, &kinesis.PutRecordsRequestEntry{
			PartitionKey: aws.String(uid),
			Data:         b,
		})
	}

	in.SetRecords(records)
	out, err := s.knsClient.PutRecords(in)
	if err != nil {
		return output, status.Errorf(codes.FailedPrecondition, err.Error())
	}
	if out.FailedRecordCount != nil && *out.FailedRecordCount > 0 {
		// look into Records ErrorCode + ErrorMessage
		failedRecords := make([]string, 0)
		for _, failedRecord := range out.Records {
			if failedRecord.ErrorCode != nil || failedRecord.ErrorMessage != nil {
				failedRecords = append(failedRecords, failedRecord.String())
			}
		}
		output.Count = len(in.Records) - len(failedRecords)
		return output, status.Errorf(
			codes.FailedPrecondition,
			strings.Join(failedRecords, "; "),
		)
	}

	output.Count = len(in.Records)

	return
}

// Delete by uuid from proto message, output with single Record in bytes
func (s *KNSStorer) Delete(ctx context.Context, input *goxstore.Input) (output *goxstore.Output, err error) {
	return nil, status.Error(codes.Unimplemented, codes.Unimplemented.String())
}

// DeleteBatch by slice of given proto messages, when batch is processed, output only shows count
func (s *KNSStorer) DeleteBatch(ctx context.Context, input *goxstore.Input) (output *goxstore.Output, err error) {
	return nil, status.Error(codes.Unimplemented, codes.Unimplemented.String())
}
